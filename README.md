# UserSightSDK

[![Pod Version](http://img.shields.io/cocoapods/v/UserSightSDK.svg?style=flat&logo=cocoapods&color=informational)](https://gitlab.com/originallyus/usersight-ios-sdk)


![Non-instrusive style](https://gitlab.com/originallyus/usersight-ios-sdk/-/raw/master/Screenshots/ss_iphone_non_instrusive_small.png)


## Installation
The SDK is self-contained and requires no external dependencies. Simply install it via cocoapod.
```javascript
pod 'UserSightSDK', '~> 0.1.0'
```
There are new interfaces & callbacks available, please refer to the comments in `UserSightSDK.h` file for exact details.




## Initialization
The SDK needs to be initialized with an secret key specific to your app Bundle ID. Please contact us to obtain App Secret specific to your app.
* Objective-C
```objc
#import "UserSightSDK.h"

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    ...
    NSString * appSecret = @"xxxxxxxxxxxxxxxx";
    [UserSightSDK initWithAppSecret:appSecret application:application];
}
```
* Swift
```swift
import UserSightSDK

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    ...
    let appSecret = "xxxxxxxxxxxxxxxx"
    UserSightSDK.initWithAppSecret(appSecret, application: application)
}
```



## Language
[Optional] Setup language used by the SDK. Supported languages:
- `en` - English (default)
- `zh` - Simplified Chinese
- `zh_tw` - Traditional Chinese
- `ms` - Malay
- `id` - Bahasa Indonesia
- `ta` - Tamil
- `th` - Thai
- `tl` - Tagalog
* Objective-C
```objc
[UserSightSDK setLanguage:@"zh"];
```
* Swift
```swift
UserSightSDK.setLanguage("th")
```
Please setup language before showing the form. When invalid or unsupported language code are used, SDK will automatically fallback to English.



## UserID
[Optional] Tag an app-specific UserID to the SDK. This is only for reporting purpose.
* Objective-C
```objc
[UserSightSDK setUserId:@"1234"];     //supports string only
```
* Swift
```swift
UserSightSDK.setUserId("a31725")      //supports string only
```
Please set UserId before showing the form.




## Metadata
[Optional] Tag an app-specific metadata to the SDK. This is only for reporting purpose. Please contact your business team for specific requirements.
* Objective-C
```objc
[UserSightSDK setMetadata:@"policy_765123"];     //supports string only
```
* Swift
```swift
UserSightSDK.setMetadata("policy_765123")        //supports string only
```
Please set metadata before showing the form.




## Show feedback form
Feedback form can be triggered from any where in your project code. It will stay always-on-top of your current UIWindow.
* Objective-C
```objc
#import "UserSightSDK.h"
...
[UserSightSDK showFeedbackFormWithEventTag:nil];
```
* Swift
```swift
import UserSightSDK
...
UserSightSDK.showFeedbackForm(withEventTag:nil)
```
#### [Optional] `EventTag`
This is normally the name of an important event that triggers the feedback form. Eg. `btn_redeem`, `level_up`, `login`, `complete_purchase`, `subscribed`, `followed`. This is for reporting purpose only. Please contact your business/admin team for specific instructions regarding Event Tags. 




## Show a specific feedback form
A specific custom form can be pre-setup by business/admin via backend and to be used inside app at specific part of the app. To show such form, please add extra `formSlug` parameter to the function call. Starting from version 0.3.0, there are 6 different *Survey Type* supported and they are differentiated via `formSlug`. Please obtain specific instructions from your business/admin team regarding this.
* Objective-C
```objc
#import "UserSightSDK.h"
...
[UserSightSDK showFeedbackFormWithEventTag:@"game_over" formSlug:@"satisfaction-1"];
```
* Swift
```swift
import UserSightSDK
...
UserSightSDK.showFeedbackForm(withEventTag:"jackpot", formSlug: "nps-1")
```




## Debug vs Production mode
In **Debug mode** `(debugFeedbackFormWithEventTag)`, the feedback forms are always shown, for debugging purpose. **DO NOT** use this function in Production/UAT/SIT environment.

In **Production mode** `(showFeedbackFormWithEventTag)`, the feedback form may not be shown all the time, ie. nothing happens when the code is executed. *This is not a bug*. The forms are configured to be shown only once per hour, per day, per week, per quarter, etc. from backend.




## Instrusive vs Non-instrusive style
This is configured on the backend (via CMS). Below are mockups of these styles on iPad.
**Instrusive style**
![Instrusive style](https://gitlab.com/originallyus/feedback-ios-sdk/-/raw/master/Screenshots/ss_ipad_instrusive.png)
**Non-instrusive style**
![Non-instrusive style](https://gitlab.com/originallyus/feedback-ios-sdk/-/raw/master/Screenshots/ss_ipad_non_instrusive.png)





## Example project
Example project in Objective-C and Swift are available via [Gitlab](https://gitlab.com/originallyus/feedback-ios-sdk/-/tree/P3)

