
Pod::Spec.new do |s|
  s.name             = 'UserSightSDK'
  s.version          = '0.2.0'
  s.summary          = 'UserSight SDK'
  s.description  = <<-DESC
An easy to use digital feedback solution.
                   DESC
  s.homepage     = "https://gitlab.com/originallyus/usersight-ios-sdk/"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "author" => "torin@originally.us" }
  
  s.platform     = :ios, "10.0"
  s.ios.deployment_target  = '10.3'

  s.source       = { :git => "https://gitlab.com/originallyus/usersight-ios-sdk.git", :tag => s.version.to_s }

  s.source_files  = 'FeedbackSDKFramework/UserSightSDK/**/*.{h,m}'
  s.public_header_files = 'FeedbackSDKFramework/UserSightSDK/**/*.h'
  s.resource_bundles = {
     'UserSightSDK' => ['FeedbackSDKFramework/UserSightSDK/**/*.{xib,ttf,png,xyz}']
  }
  s.ios.exclude_files = 'Sources/Pods'

  s.requires_arc  = true
  s.framework     = "UIKit"

end

  
