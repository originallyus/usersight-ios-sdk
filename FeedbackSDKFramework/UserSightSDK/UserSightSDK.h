//
//  UserSightSDK.h
//
//  Copyright (c) 2020 Originally US. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for the SDK.
FOUNDATION_EXPORT double UserSightSDKVersionNumber;

//! Project version string for the SDK.
FOUNDATION_EXPORT const unsigned char UserSightSDKVersionString[];


@interface UserSightSDK : NSObject


#pragma mark - Basic Interfaces

/*
 * Version number of the SDK
 */
+ (NSString * _Nonnull)version;

/*
 * Returns whether UserSightSDK is being shown
 */
+ (BOOL)isShowing;

/*
 * Returns whether UserSightSDK is configured with an App Secret key
 */
+ (BOOL)checkInitialized;

/*
 * Forcefully dismiss the rating UI
 * This may be suitable for scenarios like session timeout, session expired where host application
 * needs to forcefully dismiss any user-related UI and logout the user
 * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
 * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this feature
 * Completion block will be called as per usual after finish dismissing the UI
 * This function can be called from any thread (both background thread & main thread)
 */
+ (void)forceDismiss:(BOOL)animated;


#pragma mark - Initialization

/*
 * Initialize the SDK with provided App Secret specific to your Bundle ID
 * To be called inside application:didFinishLaunchingWithOptions: function in your AppDelegate
 * Please contact Originally US for the app secrets
 */
+ (void)initWithAppSecret:(NSString * _Nonnull)appSecret
              application:(UIApplication * _Nonnull)application;

/*
 * Shortcut function to change App Secret
 */
+ (void)setAppSecret:(NSString * _Nonnull)appSecret;

/*
 * Optional: Configure user-provided User ID
 * This is the User ID of the user logged into the host application
 * For reporting purpose only
 */
+ (void)setUserId:(NSString * _Nullable)userId;

/*
 * Optional: Change language used by the SDK
 * Please set the language before showing the form
 */
+ (void)setLanguage:(NSString * _Nonnull)lang;

/*
 * Optional: Enable/Disable always on top behaviour
 * Default: always on top
 */
+ (void)alwaysOnTop:(BOOL)enabled;


#pragma mark - Metadata

/*
 * Optional: Reset/Clear all metadata
 */
+ (void)clearMetadata;

/*
 * Optional: Set simple metadata field
 * Please set metadata before showing the form
 */
+ (void)setMetadata:(NSString * _Nullable)value;

/*
 * Optional: Set custom metadata field
 * Please set metadata before showing the form
 */
+ (void)setCustomMetadata:(NSString * _Nullable)key value:(NSString * _Nullable)value;


#pragma mark - Presentation

/*
 * This function will always trigger the feedback form, for debugging purpose.
 * Do not use this function in Staging or Production environment
 * eventTag: optional parameter, tag the form to an event in the host application. For reporting purpose only.
 */
+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag;

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                           completion:(void (^ __nullable)(BOOL didShow))completion;


/*
 * This function will always trigger the feedback form, for debugging purpose.
 * Do not use this function in Staging or Production environment
 * eventTag: optional parameter, tag the form to an event in the host application. For reporting purpose only.
 * formSlug: optional parameter, to use a different form. Please follow specific instructions from Originally US or design team, if only applicable.
 */
+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug;

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                             formSlug:(NSString * _Nullable)formSlug
                           completion:(void (^ __nullable)(BOOL didShow))completion;

/*
 * This function may not show a feedback form all the time, depends on the frequency configurations on our SDK backend.
 * eventTag: optional parameter, tag the form to an event in the host application. For reporting purpose only.
 */
+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag;

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                          completion:(void (^ __nullable)(BOOL didShow))completion;


/*
 * This function will request a form from server and display it.
 * This function may not show a feedback form all the time, depends on the frequency configurations on our SDK backend.
 * eventTag: optional parameter, tag the form to an event in the host application. For reporting purpose only.
 * formSlug: optional parameter, to use a different form. Please follow specific instructions from Originally US or design team, if only applicable.
 */
+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug;

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                            formSlug:(NSString * _Nullable)formSlug
                          completion:(void (^ __nullable)(BOOL didShow))completion;

@end
