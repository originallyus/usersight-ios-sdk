//
//  BaseModel.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OUSBaseModel : NSObject

@property (nonatomic, copy) NSNumber * ID;

- (id)initWithDictionary:(NSDictionary *)dict;
- (id)updateWithDictionary:(NSDictionary *)dict;
- (id)updateWithModel:(OUSBaseModel *)newModel;

- (NSMutableDictionary *)toDictionary;
- (NSMutableDictionary *)toDictionaryUseNullValue:(BOOL)useNull;
- (NSMutableDictionary *)toDictionaryDeep:(BOOL)useNull;

@end

NS_ASSUME_NONNULL_END
