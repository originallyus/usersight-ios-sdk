//
//  OUSTheme.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 1/8/21.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OUSTheme : OUSBaseModel

@property (nonatomic, copy) UIColor * _Nullable close_icon_color;

@property (nonatomic, copy) UIColor * _Nullable body_bg_color;
@property (nonatomic, copy) UIColor * _Nullable body_text_color;
@property (nonatomic, copy) UIColor * _Nullable title_color;
@property (nonatomic, copy) UIColor * _Nullable instruction_color;
@property (nonatomic, copy) UIColor * _Nullable question_color;
@property (nonatomic, copy) UIColor * _Nullable secondary_question_color;
@property (nonatomic, copy) NSString * _Nullable title_font;
@property (nonatomic, copy) NSString * _Nullable button_font;
@property (nonatomic, copy) NSString * _Nullable option_font;
@property (nonatomic, copy) NSString * _Nullable question_font;
@property (nonatomic, copy) NSString * _Nullable secondary_question_font;
@property (nonatomic, copy) NSString * _Nullable instruction_font;
@property (nonatomic, copy) NSString * _Nullable textarea_font;
@property (nonatomic, copy) NSString * _Nullable fineprint_font;
@property (nonatomic, copy) NSNumber * _Nullable title_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable instruction_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable question_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable secondary_question_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable option_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable fineprint_fontsize;
@property (nonatomic, copy) NSNumber * _Nullable button_fontsize;
@property (nonatomic, copy) UIColor * _Nullable fineprint_color;
@property (nonatomic, copy) UIColor * _Nullable normal_star_color;
@property (nonatomic, copy) UIColor * _Nullable selected_star_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_border_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_bg_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_text_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_selected_border_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_selected_bg_color;
@property (nonatomic, copy) UIColor * _Nullable rating_number_selected_text_color;
@property (nonatomic, copy) NSString * _Nullable rating_number_font;
@property (nonatomic, copy) NSNumber * _Nullable rating_number_fontsize;
@property (nonatomic, copy) UIColor * _Nullable option_border_color;
@property (nonatomic, copy) UIColor * _Nullable option_bg_color;
@property (nonatomic, copy) UIColor * _Nullable option_text_color;
@property (nonatomic, copy) UIColor * _Nullable option_selected_border_color;
@property (nonatomic, copy) UIColor * _Nullable option_selected_bg_color;
@property (nonatomic, copy) UIColor * _Nullable option_selected_text_color;
@property (nonatomic, copy) UIColor * _Nullable scale_label_color;
@property (nonatomic, copy) NSString * _Nullable scale_label_font;
@property (nonatomic, copy) NSNumber * _Nullable scale_label_fontsize;
@property (nonatomic, copy) UIColor * _Nullable button_text_color;
@property (nonatomic, copy) UIColor * _Nullable button_bg_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_border_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_bg_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_placeholder_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_focus_border_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_focus_bg_color;
@property (nonatomic, copy) UIColor * _Nullable textarea_text_color;
@property (nonatomic, copy) NSString * _Nullable textarea_fontsize;

@end

NS_ASSUME_NONNULL_END
