//
//  OUSFont.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 1/8/21.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OUSFont : OUSBaseModel

@property (nonatomic, copy) NSString * _Nullable name;           // "Dosis-Bold"
@property (nonatomic, copy) NSString * _Nullable file;           // "Dosis-Bold.ttf"
@property (nonatomic, copy) NSString * _Nullable file_url;       // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.ttf"

//@property (nonatomic, copy) NSString * _Nullable eot_url;        // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.eot"
//@property (nonatomic, copy) NSString * _Nullable otf_url;        // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.otf"
//@property (nonatomic, copy) NSString * _Nullable svg_url;        // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.svg"
//@property (nonatomic, copy) NSString * _Nullable ttf_url;        // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.ttf"
//@property (nonatomic, copy) NSString * _Nullable woff_url;       // "https://44854569532069212172.usersight.io/uploads/fonts/Dosis-Bold.woff"

//@property (nonatomic, copy) NSString * _Nullable base64;         // "AAEAAAAQAQAABAAARkZUTZEWbykAATWQAAAAHEdERUYdgBgGA

@end

NS_ASSUME_NONNULL_END
