//
//  Form.m
//  UserSightSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import "OUSPreload.h"
#import "NSString+OUSAdditions.h"

@implementation OUSPreload

//Override
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    if (!self)
        return nil;
     
    //Fonts
    NSMutableArray * fontModelsArray = [NSMutableArray array];
    NSArray * fontsArray = [dict objectForKey:@"fonts"];
    for (NSDictionary * fontDict in fontsArray)
        if ([fontDict isKindOfClass:NSDictionary.class]) {
            OUSFont * fontModel = [[OUSFont alloc] initWithDictionary:fontDict];
            if (fontModel)
                [fontModelsArray addObject:fontModel];
        }

    //Assign
    self.fonts = fontModelsArray;
        
    return self;
}


#pragma mark -

- (BOOL)isError
{
    if (self.error.length > 2)
        return YES;
    if (self.error != nil && [self.error isKindOfClass:NSString.class] == NO) {
        NSLog(@"!!!!%@.error is not NSString (%@)!!!!", self.class, self.error.class);
        return NO;
    }
    return NO;
}

@end
