//
//  BaseModel.m
//  UserSightSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <objc/runtime.h>
#import <UIKit/UIKit.h>
#import "OUSBaseModel.h"
#import "NSString+OUSAdditions.h"
#import "UIColor+OUSAdditions.h"

@implementation OUSBaseModel

#pragma mark -

- (id)init
{
    self = [super init];
    if (self == nil)
        return nil;
    
    //[self initCustomAccessor];
    
    return self;
}

- (id)initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self == nil)
        return nil;
    
    //[self initCustomAccessor];
    
    return [self updateWithDictionary:dict];
}

- (id)updateWithDictionary:(NSDictionary*)dict
{
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator * dictEnumerator = [dict keyEnumerator];
    id rawDictKey;
    while ((rawDictKey = [dictEnumerator nextObject]))
    {
        id dictValue = [dict objectForKey:rawDictKey];
        if (dictValue == nil)
            continue;
                
        //Skip empty values
        if ([dictValue isKindOfClass:[NSNull class]] ||
            ([dictValue isKindOfClass:[NSString class]] && [dictValue isEqualToString:@"null"])) {
            continue;
        }
        
        //Special case for lowercase "id" property (from Rails)
        if ([rawDictKey isEqualToString:@"id"])
            rawDictKey = @"ID";
        
        //Special case for lowercase "description" property (reserved word in Objective-C)
        if ([rawDictKey isEqualToString:@"description"])
            rawDictKey = @"description_alt";
        
        //--------

        //If our model has this @property it match our ivar name, then set it
        //eg. @"NSNumber" (note the explicit @ and " characters)
        NSString *ivarType = [propertiesList objectForKey:rawDictKey];
        if (ivarType == nil)
            continue;
        
        //Handle NSDate type
        if ([ivarType isEqual:@"@\"NSDate\""])
        {
            //Unix timestamp format
            if ([dictValue isKindOfClass:[NSNumber class]])
            {
                NSDate *dateTimeValue = [NSDate dateWithTimeIntervalSince1970:[dictValue longLongValue]];
                dictValue = dateTimeValue;
            }
        }
        
        //Handle UIColor type
        if ([ivarType isEqual:@"@\"UIColor\""])
        {
            NSString * colorString = [NSString stringWithFormat:@"%@", dictValue];
            UIColor * color = [UIColor ous_colorWithHexString:colorString];         //expect nil for invalid values
            dictValue = color;
        }
        
        //Handle NSString @property type being received as NSNumber from server (known PHP issue)
        if ([ivarType isEqual:@"@\"NSString\""] && [dictValue isKindOfClass:[NSNumber class]])
        {
            if ([dictValue respondsToSelector:@selector(stringValue)])
                dictValue = [dictValue stringValue];
        }
        
        //Handle PHP number/string confusion
        if ([ivarType isEqual:@"@\"NSNumber\""] && [dictValue isKindOfClass:[NSString class]])
        {
            dictValue = [NSDecimalNumber decimalNumberWithString:dictValue];
        }
        
        //Set the @property with KVO
        [self setValue:dictValue forKey:rawDictKey];
    }

    return self;
}

- (id)updateWithModel:(OUSBaseModel *)newModel
{
    if (newModel == nil)
        return self;
    
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator *dictEnumerator = [propertiesList keyEnumerator];
    id ivarName;
    while ((ivarName = [dictEnumerator nextObject]))
    {
        NSString *ivarType = [propertiesList objectForKey:ivarName];
        if (ivarType == nil)
            continue;

        id newValue = [newModel valueForKey:ivarName];
        if (newValue == nil)
            continue;
        
        [self setValue:newValue forKey:ivarName];
    }

    return self;
}


#pragma mark - Low-level runtime

+ (NSDictionary *)staticPropertiesList
{
    BOOL nonBaseModel = [self superclass] != [NSObject class];

    // Return cache only for non-BaseModel
    Class selfClassObject = [self class];
    Class superClassObject = [self superclass];
    
    // Recurse up the classes, but stop at NSObject. Each class only reports its own properties, not those inherited from its superclass
    NSMutableDictionary *theProps;
    if (nonBaseModel)
        theProps = [[NSMutableDictionary alloc] initWithDictionary:[superClassObject staticPropertiesList]];
    else
        theProps = [NSMutableDictionary dictionary];
    
    int i;
    unsigned int outCount;
    objc_property_t *propList = class_copyPropertyList(selfClassObject, &outCount);
    
    // Loop through properties and add declarations for the create
    for (i=0; i < outCount; i++)
    {
        objc_property_t * oneProp = propList + i;
        NSString *propName = [NSString stringWithUTF8String:property_getName(*oneProp)];
        if ([propName isEqualToString:@"actualValueCache"])
            continue;

        //T@"NSNumber",C,N,V_ID
        NSString *attrs = [NSString stringWithUTF8String: property_getAttributes(*oneProp)];
        NSArray *attrParts = [attrs componentsSeparatedByString:@","];
        if (attrParts == nil)
            continue;
        if ([attrParts count] <= 0)
            continue;

        NSString *propType = [[attrParts objectAtIndex:0] substringFromIndex:1];
        [theProps setObject:propType forKey:propName];
    }
    free(propList);
    
    NSDictionary * dict = [theProps copy];
    return dict;
}


#pragma mark - Output

/*
 * Print out all ivars for debugging
 */
- (NSString *)description
{
    NSDictionary *dictionary = [self toDictionaryUseNullValue:YES];
    return [NSString stringWithFormat:@"<%@ %p> %@", NSStringFromClass([self class]), self, dictionary];
}

/*
 * Pack all properties into a dictionary, without Null values
 */
- (NSMutableDictionary *)toDictionary
{
    return [self toDictionaryUseNullValue:NO];
}

/*
 * Pack all properties into a dictionary, with or without Null values
 */
- (NSMutableDictionary*)toDictionaryUseNullValue:(BOOL)useNull
{
    NSMutableDictionary * outputDictionary = [NSMutableDictionary dictionary];
    
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator * dictEnumerator = [propertiesList keyEnumerator];
    id ivarName;
    while ((ivarName = [dictEnumerator nextObject]))
    {
        //Don't convert the dynamic @properties
        if ([ivarName isEqualToString:@"dynamicDataLinkingDict"])
            continue;
        
        id ivarValue = [self valueForKey:ivarName];
        if (ivarValue == nil) {
            if (useNull == NO)
                continue;
            ivarValue = [NSNull null];
        }
        
        //Type
        NSString *ivarType = [propertiesList objectForKey:ivarName];
        if (ivarType == nil)
            continue;
        
        //Not able to save, ignore
        if ([ivarType isEqualToString:@"@\"CFType\""])
            continue;
        
        //Convert NSDate to NSNumber timestamp
        if ([ivarType isEqualToString:@"@\"NSDate\""])
            if ([ivarValue isKindOfClass:[NSDate class]])
                ivarValue = [NSNumber numberWithUnsignedLongLong:[ivarValue timeIntervalSince1970]];
        
        if ([ivarValue conformsToProtocol:@protocol(NSSecureCoding)] == NO)
            continue;

        //Store it to output dictionary
        [outputDictionary setObject:ivarValue forKey:ivarName];
    }
    
    //Extra lower-case 'id'
    //Don't do this, will cause double objects with same ID in storage
    //[outputDictionary setValidObject:self.ID forKey:@"id"];

    return outputDictionary;
}

- (NSMutableDictionary *)toDictionaryDeep:(BOOL)useNull
{
    NSMutableDictionary * outputDictionary = [NSMutableDictionary dictionary];
    
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator * dictEnumerator = [propertiesList keyEnumerator];
    id ivarName;
    while ((ivarName = [dictEnumerator nextObject]))
    {
        if ([ivarName isEqualToString:@"dynamicDataLinkingDict"])
            continue;
        if ([ivarName isEqualToString:@"dataModelLinking"])
            continue;
        if ([ivarName isEqualToString:@"internalCacheProperties"])
            continue;
        if ([ivarName isEqualToString:@"ignoredProperties"])
            continue;
        if ([ivarName isEqualToString:@"alternateIdKeys"])
            continue;
        if ([ivarName isEqualToString:@"alternateIdKey"])
            continue;
        if ([ivarName isEqualToString:@"dataLinkingInfoDict"])
            continue;

        id ivarValue = [self valueForKey:ivarName];
        if (ivarValue == nil) {
            if (useNull == NO)
                continue;
            ivarValue = [NSNull null];
        }
        
        //Type
        NSString *ivarType = [propertiesList objectForKey:ivarName];
        if (ivarType == nil)
            continue;
        
        //Not able to save, ignore
        if ([ivarType isEqualToString:@"@\"CFType\""])
            continue;
        
        //Convert NSDate to NSNumber timestamp
        if ([ivarType isEqualToString:@"@\"NSDate\""])
            if ([ivarValue isKindOfClass:[NSDate class]])
                ivarValue = [NSNumber numberWithUnsignedLongLong:[ivarValue timeIntervalSince1970]];
        
        //Recursive expand further
        if ([ivarValue respondsToSelector:NSSelectorFromString(@"toDictionaryDeep:")]) {
            ivarValue = [ivarValue toDictionaryDeep:useNull];
        }
        
        //Store it to output dictionary
        [outputDictionary setObject:ivarValue forKey:ivarName];
    }
    
    //Extra lower-case 'id'
    if (self.ID)
        [outputDictionary setObject:self.ID forKey:@"id"];

    return outputDictionary;
}

/*
 * Support offline caching with NSKeyArchiver
 */
- (void)encodeWithCoder:(NSCoder *)coder
{
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator *dictEnumerator = [propertiesList keyEnumerator];
    NSString * ivarName;
    while ((ivarName = [dictEnumerator nextObject]))
    {
        id ivarValue = [self valueForKey:ivarName];
        if (ivarValue != nil)
            [coder encodeObject:ivarValue forKey:ivarName];
    }
}

/*
 * Support offline caching with NSKeyArchiver
 */
- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self == nil)
        return self;
    
    //For each top-level property in the input dictionary
    NSDictionary * propertiesList = [[self class] staticPropertiesList];
    NSEnumerator *dictEnumerator = [propertiesList keyEnumerator];
    id ivarName;
    while ((ivarName = [dictEnumerator nextObject]))
    {
        id value = [coder decodeObjectForKey:ivarName];
        if (value != nil)
            [self setValue:value forKey:ivarName];
    }
    
    return self;
}

@end
