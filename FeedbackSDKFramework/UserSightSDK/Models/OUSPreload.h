//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"
#import "OUSFont.h"

@interface OUSPreload : OUSBaseModel

@property (nonatomic, assign) BOOL debug;
@property (nonatomic, copy) NSNumber * _Nullable app_id;

//Debug
@property (nonatomic, copy) NSString * _Nullable error;

//Dynamic property
@property (nonatomic, strong) NSMutableArray * _Nullable fonts;

@property (nonatomic, strong) NSArray * _Nullable preload_images;         // [ "https://xxx", "https://yyy" ]



#pragma mark -

- (BOOL)isError;

@end
