//
//  Form.m
//  UserSightSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import "OUSForm.h"
#import "OUSOption.h"
#import "NSString+OUSAdditions.h"

@implementation OUSForm

//Override
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    if (!self)
        return nil;
 
    //Custom handling for request_id
    self.request_id = self.ID;
    
    //Theme
    NSDictionary * themeDict = [dict objectForKey:@"theme"];
    if ([themeDict isKindOfClass:NSDictionary.class])
        self.theme = [[OUSTheme alloc] initWithDictionary:themeDict];
    
    //Fonts
    NSMutableArray * fontModelsArray = [NSMutableArray array];
    NSArray * fontsArray = [dict objectForKey:@"fonts"];
    for (NSDictionary * fontDict in fontsArray)
        if ([fontDict isKindOfClass:NSDictionary.class]) {
            OUSFont * fontModel = [[OUSFont alloc] initWithDictionary:fontDict];
            if (fontModel)
                [fontModelsArray addObject:fontModel];
        }
        
    //Options
    NSMutableArray * optionModelsArray = [NSMutableArray array];
    NSArray * optionsArray = [dict objectForKey:@"options"];
    for (NSDictionary * optionDict in optionsArray)
        if ([optionDict isKindOfClass:NSDictionary.class]) {
            OUSOption * optionModel = [[OUSOption alloc] initWithDictionary:optionDict];
            if (optionModel)
                [optionModelsArray addObject:optionModel];
        }
    
    //Randomize the options order
    //Do this only once to keep it stable for the rest of the code
    if (self.options_randomize.boolValue) {
        NSUInteger count = [optionModelsArray count];
        for (NSUInteger i = 0; i < count; ++i) {
            NSUInteger nElements = count - i;
            NSUInteger n = arc4random_uniform((u_int32_t)nElements) + i;
            [optionModelsArray exchangeObjectAtIndex:i withObjectAtIndex:n];        // Select a random element to swap with
        }
    }
    
    //Assign
    self.fonts = fontModelsArray;
    self.options = optionModelsArray;
    
    //Intermediate form
    NSDictionary * extra_form = [dict objectForKey:@"extra_form"];
    if ([extra_form isKindOfClass:NSDictionary.class])
        self.extra_form = [[OUSForm alloc] initWithDictionary:extra_form];
    
    return self;
}


#pragma mark -

- (BOOL)isError
{
    if (self.error.length > 2)
        return YES;
    if (self.error != nil && [self.error isKindOfClass:NSString.class] == NO) {
        NSLog(@"!!!!%@.error is not NSString (%@)!!!!", self.class, self.error.class);
        return NO;
    }
    return NO;
}

- (BOOL)hasValidForm
{
    if (self.title.length <= 0) {
        NSLog(@"Form is missing 'title' field");
        return NO;
    }
    if (self.theme == nil) {
        NSLog(@"Form is missing 'theme' field");
        return NO;
    }
    
    return YES;
}

- (BOOL)hasRating
{
    return self.show_rating_star.boolValue || self.show_rating_number.boolValue;
}

- (BOOL)hasOptions
{
    return self.options.count > 0;
}

- (BOOL)hasRatingOrOption
{
    return self.show_rating_star.boolValue || self.show_rating_number.boolValue || self.options.count > 0;
}

- (BOOL)hasAutoSubmitForRating:(NSInteger)rating
{
    if (self.auto_submit == nil || [self.auto_submit isKindOfClass:NSArray.class] == NO)
        return NO;
    
    for (NSNumber * num in self.auto_submit)
        if (num.integerValue == rating)
            return YES;
    
    return NO;
}

- (NSTextAlignment)text_alignment
{
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"left"])
        return NSTextAlignmentLeft;
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"right"])
        return NSTextAlignmentRight;
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"center"])
        return NSTextAlignmentCenter;
    
    return NSTextAlignmentNatural;
}

@end
