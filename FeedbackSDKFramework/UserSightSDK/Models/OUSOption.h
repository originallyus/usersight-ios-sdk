//
//  OUSOption.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 1/8/21.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"

@interface OUSOption : OUSBaseModel

@property (nonatomic, copy) NSString * _Nullable slug;
@property (nonatomic, copy) NSString * _Nullable title;

@end
