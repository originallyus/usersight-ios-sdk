//
//  Form.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"
#import "OUSFont.h"
#import "OUSTheme.h"

@interface OUSForm : OUSBaseModel

@property (nonatomic, assign) BOOL debug;
@property (nonatomic, copy) NSString * _Nullable slug;
@property (nonatomic, copy) NSString * _Nullable event_tag;
@property (nonatomic, copy) NSNumber * _Nullable request_id;

//Main styles
@property (nonatomic, copy) NSNumber * _Nullable instrusive_style;        //BOOL
@property (nonatomic, copy) NSString * _Nullable alignment;               //"center",

@property (nonatomic, copy) NSString * _Nullable image_file_url;          //https://aia-dfs.originally.us/uploads/icons/success.png

//Text
@property (nonatomic, copy) NSString * _Nullable title;                   //"HELP US GIVE YOU A BETTER EXPERIENCE"
@property (nonatomic, copy) NSString * _Nullable question;                //"Tell us about your experience today"
@property (nonatomic, copy) NSString * _Nullable instruction;             //"Please let us know what went wrong?"
@property (nonatomic, copy) NSString * _Nullable instruction_5_star;      //"Please let us know what went wrong?"
@property (nonatomic, copy) NSString * _Nullable secondary_question;      //"What is the primary reason for your score"
@property (nonatomic, copy) NSString * _Nullable secondary_question_5_star;
@property (nonatomic, copy) NSString * _Nullable comment_placeholder;     //"Let us know how we can make it better for you."
@property (nonatomic, copy) NSString * _Nullable comment_placeholder_5_star;
@property (nonatomic, copy) NSString * _Nullable fineprint;               //"(AIA may reach out to understand the feedback provided)"
@property (nonatomic, copy) NSString * _Nullable button_text;             //"Submit"
@property (nonatomic, copy) NSString * _Nullable url;                     //external survey

//Show/hide flags
@property (nonatomic, copy) NSNumber * _Nullable read_only_rating;        //1-5 or 10
@property (nonatomic, copy) NSString * _Nullable show_rating_star;        //1,
@property (nonatomic, copy) NSString * _Nullable show_rating_number;      //0,
@property (nonatomic, copy) NSString * _Nullable show_comment;            //BOOL
@property (nonatomic, copy) NSNumber * _Nullable comment_mandatory;       //BOOL

//Rating number 1-10
@property (nonatomic, copy) NSNumber * _Nullable rating_min;              //1
@property (nonatomic, copy) NSNumber * _Nullable rating_max;              //5

//Options
@property (nonatomic, copy) NSNumber * _Nullable options_randomize;       //BOOL
@property (nonatomic, copy) NSString * _Nullable options_multiselect;     //BOOL

//Debug
@property (nonatomic, copy) NSString * _Nullable error;

//Dynamic property
@property (nonatomic, strong) OUSTheme * _Nullable theme;
@property (nonatomic, strong) OUSForm * _Nullable extra_form;
@property (nonatomic, strong) NSMutableArray * _Nullable options;
@property (nonatomic, strong) NSMutableArray * _Nullable fonts;

//Success
@property (nonatomic, strong) NSArray * _Nullable auto_submit;            // [1,2,3,...10]
@property (nonatomic, copy) NSNumber * _Nullable auto_store_review;       //BOOL
@property (nonatomic, copy) NSString * _Nullable ios_review_url;
@property (nonatomic, strong) NSArray * _Nullable preload_images;         // [ "https://xxx", "https://yyy" ]



#pragma mark -

- (BOOL)isError;
- (BOOL)hasValidForm;

- (BOOL)hasRating;
- (BOOL)hasOptions;
- (BOOL)hasRatingOrOption;

- (NSTextAlignment)text_alignment;

- (BOOL)hasAutoSubmitForRating:(NSInteger)rating;

@end
