//
//  OUSFeedbackBaseVC.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 23/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OUSForm.h"
#import "OUSTheme.h"
#import "OUSOption.h"
#import "OUSPaperButton.h"

//Convenient autoresizing masks
#define AUTORESIZING_MASK_LEFT_RIGHT                (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)
#define AUTORESIZING_MASK_TOP_BOTTOM                (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin)
#define AUTORESIZING_MASK_WIDTH_HEIGHT              (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)
#define AUTORESIZING_MASK_ALL_SIDES                 (AUTORESIZING_MASK_TOP_BOTTOM | AUTORESIZING_MASK_LEFT_RIGHT)
#define AUTORESIZING_MASK_ALL                       (AUTORESIZING_MASK_WIDTH_HEIGHT | AUTORESIZING_MASK_ALL_SIDES)
#define AUTORESIZING_MASK_FIX_LEFT                  (UIViewAutoresizingFlexibleRightMargin | AUTORESIZING_MASK_TOP_BOTTOM)
#define AUTORESIZING_MASK_FIX_RIGHT                 (UIViewAutoresizingFlexibleLeftMargin | AUTORESIZING_MASK_TOP_BOTTOM)

//Different device form factors
#define IS_SHORT_SCREEN                             ([[UIScreen mainScreen] bounds].size.height <= 568)
#define IS_LONGER_SCREEN                            ([[UIScreen mainScreen] bounds].size.height > 568)
#define IS_IPAD                                     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_NOT_IPAD                                 (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)

//Macros
#define OUS_PREVENT_DOUBLE_TOUCH(v)                 static NSTimeInterval lastTouchedTimestamp = 0; \
                                                    NSTimeInterval now = [[NSDate date] timeIntervalSince1970]; \
                                                    if (lastTouchedTimestamp > 0) { \
                                                    NSTimeInterval delta = now - lastTouchedTimestamp; \
                                                    if (delta < v && delta > 0) \
                                                    return; \
                                                    } \
                                                    lastTouchedTimestamp = [[NSDate date] timeIntervalSince1970];

#define OUS_WEAK_SELF                               __weak typeof(self) weakSelf = self;
#define OUS_STRONG_SELF                             __strong typeof(self) strongSelf = weakSelf;
#define OUS_SIMULATE_PRESS                          [self.btnBehind simulatePress:[recognizer locationInView:recognizer.view]];


//Common spacing for all forms
#define UI_NON_INSTRUSIVE_ROUND_CORNERS             16
#define UI_ELEMENT_SPACING                          20
#define UI_ELEMENT_SPACING_SMALL                    10
#define UI_SIDE_MARGIN_IPAD                         64
#define UI_SIDE_MARGIN                              32
#define UI_SIDE_MARGIN_SMALL                        16
#define UI_OPTION_SPACING                           16      //horizontal spacing between option/suggestion buttons only
#define UI_NAVBAR_HEIGHT_IPHONE                     (self.isInstrusive ? 44+self.statusBarOffset : 44)
#define UI_NAVBAR_HEIGHT_IPAD                       (self.isInstrusive ? 44 : self.btnClose.ous_height)
#define UI_NAVBAR_HEIGHT                            (IS_IPAD ? UI_NAVBAR_HEIGHT_IPAD : UI_NAVBAR_HEIGHT_IPHONE)


//Common font size for all forms
#define UI_FONT_SIZE_TITLE                          (IS_NOT_IPAD ? 20 : self.model.isInstrusive ? 20 : 24)
#define UI_FONT_SIZE_SUBTITLE                       (IS_IPAD ? 20 : 17)
#define UI_FONT_SIZE_BODY                           (IS_IPAD ? 17 : 15)
#define UI_FONT_SIZE_FOOTNOTE                       (IS_IPAD ? 15 : 12)
#define UI_FONT_SIZE_SUCCESS                        (IS_NOT_IPAD ? 32 : self.model.isInstrusive ? 48 : 32)
#define UI_FONT_SIZE_OPTION                         UI_FONT_SIZE_BODY
#define UI_FONT_SIZE_BUTTON                         UI_FONT_SIZE_TITLE


@protocol OUSFeedbackInternalDelegate<NSObject>

@optional

- (void)internal_formDidDismissed:(UIViewController * _Nonnull)viewController;

@end


typedef void (^ UserSightSDKCompletionBlock)(BOOL didShow);

@interface UserSightBaseVC : UIViewController

@property (nonatomic, copy, nullable) UserSightSDKCompletionBlock completionBlock;    //for external use
@property (nonatomic, weak, nullable) id<OUSFeedbackInternalDelegate> delegate;         //for internal use

@property (nonatomic, weak) IBOutlet UIView * _Nullable navbar;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblNavbarTitle;
@property (nonatomic, weak) IBOutlet UIView * _Nullable navbarTitleContainer;
@property (nonatomic, weak) IBOutlet UIButton * _Nullable btnClose;
@property (nonatomic, weak) IBOutlet OUSPaperButton * _Nullable btnNavbarBehind;
@property (nonatomic, assign) CGFloat statusBarOffset;
@property (nonatomic, assign) CGFloat bottomBarOffset;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * _Nullable navbarTopSafeAreaConstraint;

@property (nonatomic, weak) IBOutlet UIView * _Nullable popupContainerView;

@property (nonatomic, strong) UITextField * _Nullable activeTextField;
@property (nonatomic, strong) UITextView * _Nullable activeTextView;
@property (nonatomic, assign) BOOL shouldHandleKeyboardNotification;

@property (nonatomic, strong) OUSForm * _Nullable original_model;       //before extra_form
@property (nonatomic, strong) OUSForm * _Nullable model;                //
@property (nonatomic, assign) BOOL instrusive_style;


- (UIViewController * _Nonnull)initWithNib;

- (void)setup;
- (void)clearDelegate;
- (void)applyRoundedCorners;

//Latch instrustive style value
- (BOOL)isInstrusive;
- (BOOL)isNonInstrusive;

//For non-instrusive style
- (void)introAnimation:(void (^ __nullable)(BOOL finished))completion;
- (void)outroAnimation:(void (^ __nullable)(BOOL finished))completion;

//Helper
- (void)showAlertWithTitle:(NSString *_Nullable)title message:(NSString *_Nullable)message;


#pragma mark - Actions

- (void)forceDismiss:(BOOL)animated;

- (IBAction)onBtnClose:(id _Nullable)sender;


#pragma mark - Keyboard

- (CGFloat)getCorrectKeyboardHeight:(CGSize)originalSize;
- (CGFloat)getCorrectKeyboardWidth:(CGSize)originalSize;


#pragma mark - In-App rating

- (void)setDidPromptInAppReview;
- (BOOL)didPromptInAppReview;

@end
