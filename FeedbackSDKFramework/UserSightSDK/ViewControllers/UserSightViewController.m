//
//  OUSFeedbackViewController.m
//  UserSightSDK
//

#import <StoreKit/StoreKit.h>
#import "UserSightViewController.h"
#import "OUSPaperButton.h"
#import "OUSStarRatingView.h"
#import "OUSNumberRatingView.h"
#import "OUSPassThroughView.h"
#import "UIView+OUSAdditions.h"
#import "UILabel+OUSAdditions.h"
#import "UIButton+OUSAdditions.h"
#import "UIColor+OUSAdditions.h"
#import "UIImage+OUSAdditions.h"
#import "UIFont+OUSAdditions.h"
#import "NSNumber+OUSAdditions.h"
#import "NSObject+OUSAdditions.h"
#import "NSString+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"
#import "UserSightAPI.h"


@interface UserSightViewController () <UIScrollViewDelegate, UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, assign) CGFloat spacing;
@property (nonatomic, assign) CGFloat sideMargin;
@property (nonatomic, assign) CGFloat topMargin;
@property (nonatomic, assign) BOOL instrusive;
@property (nonatomic, assign) BOOL isExpanded;
@property (nonatomic, strong) NSMutableDictionary * preloadedImages;
@property (nonatomic, copy) NSURL * pendingExternalUrl;

@property (nonatomic, weak) IBOutlet UIImageView * imgImage;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblTitle;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblQuestion;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblInstruction;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblSecondaryQuestion;
@property (nonatomic, weak) IBOutlet UILabel * _Nullable lblFineprint;

@property (nonatomic, assign) BOOL didTapOnRatingOrOption;
@property (nonatomic, strong) OUSStarRatingView * starRatingView;
@property (nonatomic, weak) IBOutlet UIView * starRatingViewPlaceholder;

@property (nonatomic, weak) IBOutlet UIView * numberRatingViewPlaceholder;
@property (nonatomic, strong) OUSNumberRatingView * numberRatingView;

@property (nonatomic, weak) IBOutlet UIView * optionsContainerView;
@property (nonatomic, strong) NSMutableArray * optionsViewsArray;

@property (nonatomic, weak) IBOutlet UIView * textAreaContainerView;
@property (nonatomic, weak) IBOutlet UILabel * lblCommentPlaceholder;
@property (nonatomic, weak) IBOutlet UITextView * txtComment;

@property (nonatomic, assign) BOOL isKeyboardShowing;
@property (nonatomic, assign) CGRect selfViewFrameBeforeKeyboard;
@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@property (nonatomic, weak) IBOutlet UIView * btnSubmitContainer;
@property (nonatomic, weak) IBOutlet OUSPaperButton * btnSubmit;
@property (nonatomic, assign) BOOL didSubmit;

@property (nonatomic, strong) IBOutletCollection(UIView) NSArray * sideMarginControls;

@end

@implementation UserSightViewController


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Load custom fonts in background thread
    __weak typeof (self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
        [weakself loadCustomFonts];
    });

    [self setupUIInstrusiveStyle];
    [self setupUIWithModel];
}

- (void)loadCustomFonts
{
    //Sanity check
    if (!self.model.fonts)
        return;
    
    for (OUSFont * font in self.model.fonts)
        if ([font isKindOfClass:OUSFont.class])
            [UIFont ous_loadFontName:font.name filename:font.file url:font.file_url];
}

- (void)setupUIInstrusiveStyle
{
    self.popupContainerView.tag = OUS_PASS_THROUGH_VIEW_TAGS;
    
    //Dark overlay for instrusive style
    self.view.backgroundColor = self.isInstrusive ? [UIColor.blackColor colorWithAlphaComponent:0.5] : UIColor.clearColor;
    
    //Allow touch to pass through self.view for non-instrusive style
    if ([self.view isKindOfClass:OUSPassThroughView.class])
        [(OUSPassThroughView *)self.view allowPassThrough:self.isNonInstrusive];
    
    //Reset logic flags, just to be sure
    self.didTapOnRatingOrOption = NO;
    self.didSubmit = NO;
    self.isExpanded = NO;               //for iPad/Desktop non-instrusive only
    self.pendingExternalUrl = nil;
    self.preloadedImages = nil;
        
    //Navbar height
    self.navbar.ous_height = UI_NAVBAR_HEIGHT;
    self.navbarTitleContainer.ous_height = MIN(self.navbar.ous_height, self.navbarTitleContainer.ous_height);
    if (self.isNonInstrusive)
        self.navbarTitleContainer.frame = self.navbarTitleContainer.superview.bounds;
    else
        self.navbarTitleContainer.ous_bottom = self.navbarTitleContainer.superview.ous_height;
    
    //Vertical spacing between UI elements
    self.spacing = (!IS_IPAD && IS_SHORT_SCREEN) ? UI_ELEMENT_SPACING_SMALL : UI_ELEMENT_SPACING;
    
    //Side margin
    if (IS_IPAD && self.isInstrusive)             self.sideMargin = UI_SIDE_MARGIN_IPAD;      //iPad instrusive
    else if (IS_SHORT_SCREEN)                           self.sideMargin = UI_SIDE_MARGIN_SMALL;     //small iPhone 5/5s/SE 1st gen
    else                                                self.sideMargin = UI_SIDE_MARGIN;           //normal iPhone
    for (UIView * subview in self.sideMarginControls) {
        subview.ous_left = self.sideMargin;
        subview.ous_width = subview.superview.ous_width - 2 * self.sideMargin;
    }
    
    //The very top margin
    if (IS_IPAD && self.isInstrusive)             self.topMargin = self.sideMargin;
    else if (IS_IPAD && self.isNonInstrusive)     self.topMargin = self.spacing;
    else                                                self.topMargin = self.spacing * 2;
        
    //iPad
    if (IS_IPAD)
    {
        //Popup instrustive style
        if (self.isInstrusive)
        {
            self.popupContainerView.ous_width = 768;
            self.popupContainerView.ous_height = 294;       //initial size (stars not selected)
            self.popupContainerView.center = self.view.ous_contentCenter;
            self.popupContainerView.autoresizingMask = AUTORESIZING_MASK_ALL_SIDES;
            [self.popupContainerView ous_roundCornerWithRadius:8];
        }
        //Non-instrusive small bar at the bottom
        else
        {
            self.popupContainerView.ous_width = 375;
            self.popupContainerView.ous_height = 294;       //initial size (stars not selected)
            self.popupContainerView.ous_left = 0.85 * (self.popupContainerView.superview.ous_width - self.popupContainerView.ous_width);
            self.popupContainerView.ous_bottom = self.popupContainerView.superview.ous_height;
            self.popupContainerView.autoresizingMask = AUTORESIZING_MASK_LEFT_RIGHT | UIViewAutoresizingFlexibleTopMargin;
            [self applyRoundedCorners];
        }
                
        //Navbar title: vertically align
        self.lblNavbarTitle.text = nil;
        self.lblNavbarTitle.ous_centerY = self.lblNavbarTitle.superview.ous_contentCenter.y;
        self.lblNavbarTitle.autoresizingMask = AUTORESIZING_MASK_ALL;
        
        //Close button, always fix to the right
        if (!self.isInstrusive)
            self.btnClose.transform = CGAffineTransformMakeRotation(M_PI_4);            // (+) icon
    }
    //iPhone
    else
    {
        if (self.isInstrusive)
        {
            self.popupContainerView.frame = self.view.bounds;
            self.popupContainerView.autoresizingMask = AUTORESIZING_MASK_WIDTH_HEIGHT;
        }
        else
        {
            self.popupContainerView.frame = self.view.bounds;
            self.popupContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
            [self applyRoundedCorners];
        }
    }
}

- (void)setupUIWithModel
{
    //Top margin
    CGFloat top = (IS_NOT_IPAD && self.isNonInstrusive) ? 0 : self.topMargin;
    self.imgImage.ous_top = top;
    self.lblQuestion.ous_top = top;
    
    //Submit button (inside its own container view)
    self.btnSubmit.ous_top = self.sideMargin / 2;
    self.btnSubmit.ous_left = self.sideMargin;
    self.btnSubmit.ous_width = self.btnSubmit.superview.ous_width - 2 * self.sideMargin;
    if (IS_IPAD && self.isInstrusive) {
        self.btnSubmit.autoresizingMask = AUTORESIZING_MASK_LEFT_RIGHT | UIViewAutoresizingFlexibleBottomMargin;
        self.btnSubmit.ous_width = 240;
        self.btnSubmit.ous_centerX = self.btnSubmit.superview.ous_contentCenter.x;
    }
    [self.btnSubmit ous_roundCornerWithRadius:8];
    
    //Submit button container
    self.btnSubmitContainer.ous_height = self.btnSubmit.ous_height + self.sideMargin + self.bottomBarOffset;
    self.btnSubmitContainer.ous_bottom = self.btnSubmitContainer.superview.ous_height;
    self.btnSubmitContainer.alpha = 0;
        
    //Adjust big scrollview
    self.scrollView.ous_top = self.navbar.ous_bottom;
    self.scrollView.ous_height = self.btnSubmitContainer.ous_top - self.scrollView.ous_top;
    
    //Tap on navbar (iPad/Desktop, non-instrusive style)
    [self.navbar ous_addTapGestureWithTarget:self action:@selector(onNavbarTap:)];
    
    //Tap to dismiss keyboard
    [self.view ous_addTapGestureWithTarget:self action:@selector(onTap:)];
    
    //Rating stars
    self.starRatingView = [[OUSStarRatingView alloc] initWithFrame:self.starRatingViewPlaceholder.bounds];
    self.starRatingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.starRatingView.backgroundColor = [UIColor clearColor];
    self.starRatingView.tintColor = self.model.theme.selected_star_color;
    self.starRatingView.emptyStarColor = self.model.theme.normal_star_color;
    self.starRatingView.starBorderWidth = 0;
    self.starRatingView.maximumValue = self.model.rating_max.integerValue;
    self.starRatingView.minimumValue = self.model.rating_min.integerValue;
    self.starRatingView.value = 0;
    self.starRatingView.allowsHalfStars = NO;
    self.starRatingView.accurateHalfStars = NO;
    [self.starRatingView addTarget:self action:@selector(didChangeStarRatingValue:) forControlEvents:UIControlEventValueChanged];
    [self.starRatingViewPlaceholder addSubview:self.starRatingView];

    //Star colors
    NSBundle * bundle = [NSBundle ous_podBundleWithClass:self.classForCoder];
    UIImage * starImage = [UIImage imageNamed:@"feedback-sdk-new-star" inBundle:bundle compatibleWithTraitCollection:nil];
    self.starRatingView.emptyStarImage = [starImage ous_changeColor:self.model.theme.normal_star_color];
    self.starRatingView.filledStarImage = [starImage ous_changeColor:self.model.theme.selected_star_color];

    //Rating number
    self.numberRatingView = [[OUSNumberRatingView alloc] initWithFrame:self.numberRatingViewPlaceholder.bounds];
    self.numberRatingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.numberRatingView.backgroundColor = [UIColor clearColor];
    self.numberRatingView.theme = self.model.theme;
    self.numberRatingView.maximumValue = self.model.rating_max.integerValue;
    self.numberRatingView.minimumValue = self.model.rating_min.integerValue;
    self.numberRatingView.value = 0;
    [self.numberRatingViewPlaceholder addSubview:self.numberRatingView];
    OUS_WEAK_SELF
    [self.numberRatingView ous_setCallbackBlock:^(id _Nonnull object) {
        OUS_STRONG_SELF
        [strongSelf didChangeStarRatingValue:object];
    }];

    //Options
    self.optionsViewsArray = [NSMutableArray array];
    [self.optionsContainerView ous_removeAllSubviews];

    //Business logics
    [self relayout:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Immediately show the options (if any) when there's no rating to be selected
    if (self.model.hasRating == NO)
        [self constructOptionsButtons:YES];
    
    //Business logics
    [self relayout:YES];
    
    //Preload images, if any
    [self preloadImagesAsync:self.model.preload_images];
}


#pragma mark - Layout

- (void)relayout:(BOOL)animated
{
    //Background colors according to model
    self.popupContainerView.backgroundColor = UIColor.clearColor;           //because of top rounded corners
    self.navbar.backgroundColor = self.model.theme.body_bg_color;
    self.scrollView.backgroundColor = self.model.theme.body_bg_color;
    self.imgImage.backgroundColor = UIColor.clearColor;
    self.optionsContainerView.backgroundColor = self.model.theme.body_bg_color;
    self.textAreaContainerView.backgroundColor = self.model.theme.body_bg_color;
    self.starRatingViewPlaceholder.backgroundColor = UIColor.clearColor;
    self.numberRatingViewPlaceholder.backgroundColor = UIColor.clearColor;
    self.btnSubmitContainer.backgroundColor = self.model.theme.body_bg_color;
    
    //Navbar
    self.btnClose.tintColor = self.model.theme.close_icon_color;
    self.btnClose.alpha = (self.model.theme.close_icon_color) ? 1 : 0;
    self.btnNavbarBehind.tapCircleColor = [self.lblTitle.textColor colorWithAlphaComponent:0.1];
    
    //Navbar title
    self.lblNavbarTitle.font = [UIFont fontWithName:self.model.theme.title_font size:self.model.theme.title_fontsize.floatValue];
    self.lblNavbarTitle.text = self.model.title;
    self.lblNavbarTitle.textColor = self.model.theme.title_color;
    self.lblNavbarTitle.minimumScaleFactor = 0.3;
    
    //Title
    UIFont * titleFont = [UIFont fontWithName:self.model.theme.title_font size:self.model.theme.title_fontsize.floatValue];
    self.lblTitle.font = titleFont;
    self.lblTitle.text = self.model.title;
    self.lblTitle.textColor = self.model.theme.title_color;
    self.lblTitle.textAlignment = self.model.text_alignment;
    [self.lblTitle ous_sizeToFitKeepWidth];
    
    //Dual title logics
    if (IS_IPAD && self.isNonInstrusive)        [self.lblNavbarTitle ous_fadeToAlpha:self.isExpanded ? 0 : 1];
    else                                        self.lblNavbarTitle.alpha = 0;
    
    //Question
    self.lblQuestion.font = [UIFont fontWithName:self.model.theme.question_font size:self.model.theme.question_fontsize.floatValue];
    self.lblQuestion.text = self.model.question;
    self.lblQuestion.textColor = self.model.theme.question_color;
    self.lblQuestion.textAlignment = self.model.text_alignment;
    [self.lblQuestion ous_sizeToFitKeepWidth];
    
    //Instruction
    self.lblInstruction.font = [UIFont fontWithName:self.model.theme.instruction_font size:self.model.theme.instruction_fontsize.floatValue];
    self.lblInstruction.textColor = self.model.theme.instruction_color;
    self.lblInstruction.textAlignment = self.model.text_alignment;
    self.lblInstruction.text = self.model.instruction;
    if (self.starRatingView.value >= 5 && self.model.instruction_5_star.length > 1)
        self.lblInstruction.text = self.model.instruction_5_star;
    [self.lblInstruction ous_sizeToFitKeepWidth];
    if (self.model.hasRating)           self.lblInstruction.alpha = self.didTapOnRatingOrOption ? 1 : 0;
    else                                self.lblInstruction.alpha = 1;

    //Secondary question
    self.lblSecondaryQuestion.font = [UIFont fontWithName:self.model.theme.secondary_question_font size:self.model.theme.secondary_question_fontsize.floatValue];
    self.lblSecondaryQuestion.textColor = self.model.theme.secondary_question_color;
    self.lblSecondaryQuestion.textAlignment = self.model.text_alignment;
    self.lblSecondaryQuestion.text = self.model.secondary_question;
    if (self.starRatingView.value >= 5 && self.model.secondary_question_5_star.length > 1)
        self.lblSecondaryQuestion.text = self.model.secondary_question_5_star;
    if (self.model.hasRatingOrOption)   self.lblSecondaryQuestion.alpha = self.didTapOnRatingOrOption ? 1 : 0;
    else                                self.lblSecondaryQuestion.alpha = 1;
    [self.lblSecondaryQuestion ous_sizeToFitKeepWidth];

    //Fineprint (non-empty only when UserId provided)
    self.lblFineprint.font = [UIFont fontWithName:self.model.theme.fineprint_font size:self.model.theme.fineprint_fontsize.floatValue];
    self.lblFineprint.text = self.model.fineprint;
    self.lblFineprint.textColor = self.model.theme.fineprint_color;
    self.lblFineprint.textAlignment = self.model.text_alignment;
    [self.lblFineprint ous_sizeToFitKeepWidth];
    if (self.model.hasRatingOrOption)   self.lblFineprint.alpha = self.didTapOnRatingOrOption ? 1 : 0;
    else                                self.lblFineprint.alpha = 1;

    //Text area placeholder
    self.lblCommentPlaceholder.font = [UIFont fontWithName:self.model.theme.textarea_font size:self.model.theme.textarea_fontsize.floatValue];
    self.lblCommentPlaceholder.textColor = self.model.theme.textarea_placeholder_color;
    self.lblCommentPlaceholder.text = self.model.comment_placeholder;
    if (self.starRatingView.value >= 5 && self.model.comment_placeholder_5_star.length > 1)
        self.lblCommentPlaceholder.text = self.model.comment_placeholder_5_star;
    [self.lblCommentPlaceholder ous_sizeToFitKeepWidth];
    
    //Text area
    self.txtComment.textColor = self.model.theme.textarea_text_color;
    self.txtComment.font = [UIFont fontWithName:self.model.theme.textarea_font size:self.model.theme.textarea_fontsize.floatValue];
    self.txtComment.contentInset = UIEdgeInsetsMake(0, 3, 0, 3);
    
    //Text area container
    NSInteger numRows = IS_SHORT_SCREEN ? 4 : 6;
    BOOL isActive = [self.txtComment isFirstResponder];
    self.textAreaContainerView.ous_height = self.lblCommentPlaceholder.font.pointSize * numRows;
    self.textAreaContainerView.layer.borderWidth = 1;
    self.textAreaContainerView.layer.borderColor = isActive ? self.model.theme.textarea_focus_border_color.CGColor : self.model.theme.textarea_border_color.CGColor;
    self.textAreaContainerView.backgroundColor = isActive ? self.model.theme.textarea_focus_bg_color : self.model.theme.textarea_bg_color;
    [self.textAreaContainerView ous_roundCornerWithRadius:4];
        
    //Show/hide text area container
    BOOL formShowComment = self.model.show_comment.boolValue;
    if (self.model.hasRatingOrOption)   self.textAreaContainerView.alpha = (formShowComment && self.didTapOnRatingOrOption) ? 1 : 0;
    else                                self.textAreaContainerView.alpha = formShowComment ? 1 : 0;

    //Submit button
    self.btnSubmit.backgroundColor = self.model.theme.button_bg_color;
    self.btnSubmit.tapCircleColor = [self.model.theme.button_text_color colorWithAlphaComponent:0.25];
    self.btnSubmit.titleLabel.font = [UIFont fontWithName:self.model.theme.button_font size:self.model.theme.button_fontsize.floatValue];
    [self.btnSubmit setTitleColor:self.model.theme.button_text_color forState:UIControlStateNormal];
    [self.btnSubmit ous_fadeToText:self.model.button_text];
    
    //Image
    self.imgImage.image = nil;
    self.imgImage.alpha = [self.model.image_file_url ous_isHttpUrl] ? 1 : 0;
    if (self.imgImage.alpha > 0)
        [self loadImageAsync:self.model.image_file_url];
    
    //Show/hide star rating
    self.starRatingViewPlaceholder.userInteractionEnabled = self.model.show_rating_star.boolValue;
    self.starRatingViewPlaceholder.alpha = self.model.show_rating_star.boolValue ? 1 : 0;
    if (self.model.read_only_rating.integerValue > 0) {
        self.starRatingViewPlaceholder.userInteractionEnabled = NO;
        self.starRatingView.value = self.model.read_only_rating.integerValue;
    }
    
    //Show/hide number rating
    self.numberRatingViewPlaceholder.userInteractionEnabled = self.model.show_rating_number.boolValue;
    self.numberRatingViewPlaceholder.alpha = self.model.show_rating_number.boolValue ? 1 : 0;
    if (self.model.read_only_rating.integerValue > 0) {
        self.numberRatingViewPlaceholder.userInteractionEnabled = NO;
        self.numberRatingView.value = self.model.read_only_rating.integerValue;
    }
    
    //Show/hide options container
    self.optionsContainerView.alpha = self.model.options.count > 0 && self.optionsContainerView.subviews.count > 0;
    
    //Smaller starting margin when keyboard is shown
    CGFloat lastBottom = 0;
    CGFloat spacing = self.spacing;
    CGFloat animationDuration = (animated ? 0.3 : 0);
    
    //Show submit button
    // • When there's rating or options, wait for it to be selected
    // • Other types, show submit button immediately
    BOOL shouldShowSubmit = self.didTapOnRatingOrOption || (self.model.hasRatingOrOption == NO);

    //Special collapsed mode
    if (IS_IPAD && self.isNonInstrusive && !self.isExpanded)
        shouldShowSubmit = NO;
    
    [self.btnSubmitContainer ous_fadeToAlpha:(shouldShowSubmit ? 1 : 0) withDuration:animationDuration];
    self.btnSubmitContainer.ous_bottom = self.btnSubmitContainer.superview.ous_height;
    BOOL isSubmitButtonVisible = self.btnSubmitContainer.superview != nil && self.btnSubmitContainer.ous_visible && self.btnSubmitContainer.alpha > 0;
    
    //Arrange all UI components within scrollview
    NSArray * array = @[self.imgImage, self.lblTitle, self.lblQuestion, self.starRatingViewPlaceholder, self.numberRatingViewPlaceholder, self.lblInstruction, self.optionsContainerView, self.lblSecondaryQuestion, self.textAreaContainerView, self.lblFineprint];
    for (UIView * subview in array)
        if (subview.ous_visible && subview.alpha > 0 && subview.ous_height > 2) {
            CGFloat top = (lastBottom <= 0) ? 0 : lastBottom + spacing;
            lastBottom = top + subview.ous_height;
            [subview ous_animateTopToValue:top duration:animationDuration delay:0];
        }
    
    //Logics for separation of btnSubmit container
    BOOL separateSubmitContainer = self.isNonInstrusive || IS_NOT_IPAD;
    self.scrollView.contentSize = CGSizeMake(1, lastBottom + (separateSubmitContainer ? self.topMargin : 0));
    
    //Special case for iPad non-instrusive
    if (IS_IPAD && self.isNonInstrusive && self.isExpanded == NO)
        self.scrollView.contentSize = CGSizeZero;
    
    //Resize container view
    OUS_WEAK_SELF
    if (IS_IPAD)
    {
        if (self.isInstrusive)
        {
            CGFloat popupHeight = self.scrollView.ous_top + self.scrollView.contentSize.height;
            if (isSubmitButtonVisible)                  popupHeight += self.btnSubmitContainer.ous_height;
            else                                        popupHeight += self.topMargin;
            
            if (animated)
            {
                [UIView animateWithDuration:animationDuration
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                    weakSelf.popupContainerView.ous_height = popupHeight;
                    weakSelf.popupContainerView.ous_centerY = weakSelf.popupContainerView.superview.ous_contentCenter.y;
                } completion:nil];
            }
            else
            {
                self.popupContainerView.ous_height = popupHeight;
                self.popupContainerView.ous_centerY = self.popupContainerView.superview.ous_contentCenter.y;
            }
        }
        else
        {
            CGFloat popupHeight = self.scrollView.ous_top + self.scrollView.contentSize.height;
            if (isSubmitButtonVisible)                  popupHeight += self.btnSubmitContainer.ous_height;
            
            if (animated)
            {
                [UIView animateWithDuration:animationDuration
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                    weakSelf.popupContainerView.ous_height = popupHeight;
                    weakSelf.popupContainerView.ous_bottom = weakSelf.popupContainerView.superview.ous_height;
                    [weakSelf applyRoundedCorners];
                } completion:nil];
            }
            else
            {
                self.popupContainerView.ous_height = popupHeight;
                self.popupContainerView.ous_bottom = self.popupContainerView.superview.ous_height;
                [weakSelf applyRoundedCorners];
            }
        }
    }
    //iPhone non-instrusive
    else if (self.isNonInstrusive)
    {
        CGFloat popupHeight = self.scrollView.ous_top + self.scrollView.contentSize.height;
        if (isSubmitButtonVisible)                  popupHeight += self.btnSubmitContainer.ous_height;
        
        [weakSelf applyRoundedCorners];
        
        if (animated)
        {
            [UIView animateWithDuration:animationDuration
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                weakSelf.popupContainerView.ous_height = popupHeight;
                weakSelf.popupContainerView.ous_bottom = weakSelf.popupContainerView.superview.ous_height;
                [weakSelf applyRoundedCorners];
            } completion:nil];
        }
        else
        {
            self.popupContainerView.ous_height = popupHeight;
            self.popupContainerView.ous_bottom = self.popupContainerView.superview.ous_height;
            [weakSelf applyRoundedCorners];
        }
    }
    
    //Scrollview height depends on whether btnSubmit is visible
    CGFloat scrollViewHeight = (isSubmitButtonVisible ? self.btnSubmitContainer.ous_top : self.scrollView.superview.ous_height) - self.scrollView.ous_top;
    [self.scrollView ous_animateHeightToValue:scrollViewHeight duration:animationDuration delay:0];
    self.scrollView.scrollEnabled = self.scrollView.contentSize.height > scrollViewHeight;
}


#pragma mark - Image

- (void)preloadImagesAsync:(NSArray *)urlsArray
{
    if ([urlsArray isKindOfClass:NSArray.class] == NO)
        return;
    if (urlsArray.count <= 0)
        return;
    
    if (self.preloadedImages.count <= 0)
        self.preloadedImages = [NSMutableDictionary dictionary];
    
    //Background thread
    OUS_WEAK_SELF
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString * urlString in urlsArray) {
            NSURL * url = [NSURL URLWithString:urlString];
            if (url == nil)
                return;
            
            NSData * data = [NSData dataWithContentsOfURL:url];
            UIImage * image = [[UIImage alloc] initWithData:data];
            if (image == nil)
                return;
            
            //Cache it
            OUS_STRONG_SELF
            [strongSelf.preloadedImages setObject:image forKey:urlString];
        }
    });
}

- (void)loadImageAsync:(NSString *)urlString
{
    if ([urlString ous_isHttpUrl] == NO)
        return;
    
    //Try to load from cache first
    UIImage * preloadedImage = [self.preloadedImages objectForKey:urlString];
    if ([preloadedImage isKindOfClass:UIImage.class]) {
        self.imgImage.image = preloadedImage;
        return;
    }
    
    //Background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL * url = [NSURL URLWithString:urlString];
        if (url == nil)
            return;
        NSData * data = [NSData dataWithContentsOfURL:url];
        UIImage * image = [[UIImage alloc] initWithData:data];
        if (image == nil)
            return;
        
        //Main thread
        OUS_WEAK_SELF
        dispatch_async(dispatch_get_main_queue(), ^{
            OUS_STRONG_SELF
            [strongSelf.imgImage setImage:image];
        });
    });
}


#pragma mark - Keyboard

//Override
- (void)onKeyboardWillShowNotification:(NSNotification *)sender
{
    self.isKeyboardShowing = YES;
    
    //If the keyboard is not triggered by a control outside Feedback SDK, hide FeedbackSDK
    if (self.view.ous_hasFirstResponder == NO)
        [self.view ous_fadeOutWithDuration:0.15];
    
    //This if-condition prevent false saving when switching focus between textfields
    if (self.selfViewFrameBeforeKeyboard.size.width <= 0)
        self.selfViewFrameBeforeKeyboard = self.view.frame;
    
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat kbHeight = [self getCorrectKeyboardHeight:kbSize];
    UIViewAnimationOptions animationCurve = [[[sender userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    //Use an absolute calculation, not relative calculation to current size
    //in order to handle international & autocompletion box correctly
    CGFloat height = self.selfViewFrameBeforeKeyboard.size.height - kbHeight;
    
    __weak typeof (self) weakSelf = self;
    CGRect finalFrame = CGRectMake(self.view.ous_top, self.view.ous_left, self.view.ous_width, height);
    [UIView animateWithDuration:duration delay:0 options:animationCurve animations:^{
        weakSelf.view.frame = finalFrame;
        
        //Scroll to bottom
        CGFloat bottomOffset = weakSelf.scrollView.contentSize.height - weakSelf.scrollView.bounds.size.height + weakSelf.scrollView.contentInset.bottom;
        if (bottomOffset < 0)
            bottomOffset = 0;
        
        CGPoint offsetPoint = CGPointMake(0, bottomOffset);
        [weakSelf.scrollView setContentOffset:offsetPoint animated:NO];
    } completion:nil];
    
    //Smaller spacing for non-iPad form factor
    self.spacing = IS_IPAD ? UI_ELEMENT_SPACING : UI_ELEMENT_SPACING_SMALL;
    
    //Hmm. why need this?
    //[self constructOptionsButtons:YES];
    
    [self relayout:YES];
}

//Override
- (void)onKeyboardWillHideNotification:(NSNotification *)sender
{
    self.isKeyboardShowing = NO;
    
    //Re-show Feedback SDK
    [self.view ous_fadeInWithDuration:0.35];
    
    //Make a copy
    CGRect finalFrame = CGRectMake(self.selfViewFrameBeforeKeyboard.origin.x,
                                   self.selfViewFrameBeforeKeyboard.origin.y,
                                   self.selfViewFrameBeforeKeyboard.size.width,
                                   self.selfViewFrameBeforeKeyboard.size.height);
    if (finalFrame.size.width <= 0)
        finalFrame.size.width = UIScreen.mainScreen.bounds.size.width;
    if (finalFrame.size.height <= 0)
        finalFrame.size.height = UIScreen.mainScreen.bounds.size.height;
    
    //Clear it
    self.selfViewFrameBeforeKeyboard = CGRectZero;
    
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animationCurve = [[[sender userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    //Restore the original self.view frame
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:duration delay:0 options:animationCurve animations:^{
        weakSelf.view.frame = finalFrame;
    } completion:nil];
    
    //568: iPhone 5/5s/SE 1st gen, 667: iPhone SE 2nd Gen
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    self.spacing = (!IS_IPAD && screenHeight <= 568) ? UI_ELEMENT_SPACING_SMALL : UI_ELEMENT_SPACING;
    
    //Hmm. why need this?
    //[self constructOptionsButtons:YES];
    
    [self relayout:YES];
}

//Override
- (void)onKeyboardDidHideNotification:(NSNotification *)sender
{
    //568: iPhone 5/5s/SE 1st gen, 667: iPhone SE 2nd Gen
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    self.spacing = (!IS_IPAD && screenHeight <= 568) ? UI_ELEMENT_SPACING_SMALL : UI_ELEMENT_SPACING;
    
    self.isKeyboardShowing = NO;
    self.activeTextView = nil;
    
    //Hmm. why need this?
    //[self constructOptionsButtons:YES];
    
    [self relayout:YES];
}



#pragma mark - Text Area

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //Placeholder
    NSString * newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    BOOL showPlaceholder = newString.length <= 0;
    CGFloat placeholderAlpha = showPlaceholder ? 1 : 0;
    [self.lblCommentPlaceholder ous_fadeToAlpha:placeholderAlpha withDuration:0.2];
    
    //Adjust height
    //[self.txtComment sizeToFitKeepWidth];
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    
}


#pragma mark - Option Buttons

- (void)constructOptionsButtons:(BOOL)animated
{
    //Sanity check
    if (self.model.options.count <= 0) {
        for (UIView * subview in self.optionsContainerView.subviews)
            [subview ous_fadeOutWithDuration:0.15 andRemoveFromSuperView:YES completion:nil];
        return;
    }
    
    //Note: options_randomize is automatically applied during OUSForm construction
    
    UIView * lastView = nil;
    for (OUSOption * option in self.model.options)
    {
        OUSPaperButton * button = [self constructSingleOptionButton:option lastView:lastView];
        if (button == nil)
            continue;
        
        lastView = button;
    }
    
    //Re-arrange (center) all buttons
    for (UIView * button in self.optionsViewsArray)
        [self rearrangeButonsOnLastLine:button.ous_top];
    
    [self.optionsContainerView ous_animateHeightToValue:lastView.ous_bottom
                                               duration:(animated ? 0.25 : 0)
                                                  delay:0];
}

- (OUSPaperButton *)constructSingleOptionButton:(OUSOption *)option lastView:(UIView *)lastView
{
    //Sanity check
    NSString * title = [option.title ous_trim];
    if (title.length <= 0)
        return nil;
    
    //Starting space according to screen height (568: iPhone 5/5s/SE 1st gen)
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    CGFloat sideSpacing = UI_OPTION_SPACING;
    
    CGFloat spacing = (screenHeight <= 568) ? (self.spacing) : (self.spacing - 6);
    spacing = MAX(spacing, 3);
    
    //Check for existing button
    OUSPaperButton * button = nil;
    for (OUSPaperButton * btn in self.optionsViewsArray)
        if ([option isEqual:[btn ous_getTagObject]])
            button = btn;
    
    //Create new
    if (button == nil) {
        button = [OUSPaperButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(lastView.ous_right, lastView.ous_top, 100, 32);
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        button.layer.borderWidth = 1;
        button.layer.borderColor = self.model.theme.option_text_color.CGColor;          //for normal state; for selected state to be changed in onBtnOption:
        button.backgroundColor = self.model.theme.option_bg_color;                      //for normal state; for selected state to be changed in onBtnOption:
        [button setTitleColor:self.model.theme.option_text_color forState:UIControlStateNormal];
        [button setTitleColor:self.model.theme.option_selected_text_color forState:UIControlStateSelected];
        
        button.titleLabel.font = [UIFont fontWithName:self.model.theme.option_font size:self.model.theme.option_fontsize.floatValue];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.tapCircleColor = [self.model.theme.option_text_color colorWithAlphaComponent:0.2];
        button.alpha = 0.1;
        [button setTitle:title forState:UIControlStateNormal];
        [button ous_tagObject:option];
        [button addTarget:self action:@selector(onBtnOption:) forControlEvents:UIControlEventTouchUpInside];
        [self.optionsViewsArray addObject:button];
    }
    [self.optionsContainerView addSubview:button];
    
    [button ous_sizeToFitKeepHeight];
    button.ous_width += sideSpacing * 2;      //padding
    [button ous_roundCorner];
    
    //No last view
    if (lastView == nil) {
        button.ous_top = 0;
        button.ous_left = 0;
        button.alpha = 1;
        return button;
    }
    
    CGFloat top = lastView.ous_top;
    CGFloat left = lastView.ous_right + sideSpacing;
    CGFloat right = left + button.ous_width;
    
    //New line
    BOOL onNewLine = (right > button.superview.ous_width);
    if (onNewLine) {
        top += button.ous_height + spacing;
        left = 0;
    }
    
    button.ous_top = top;
    button.ous_left = left;
    button.alpha = 1;
    return button;
}

- (void)rearrangeButonsOnLastLine:(CGFloat)lastViewTop
{
    CGFloat left = 99999999;
    CGFloat right = 0;
    NSMutableArray * previousButtons = [NSMutableArray array];
    for (UIView * subview in self.optionsViewsArray)
        if (subview.alpha > 0 && subview.ous_visible)
            if (subview.ous_top == lastViewTop) {
                [previousButtons addObject:subview];
                left = MIN(left, subview.ous_left);
                right = MAX(right, subview.ous_right);
            }
    
    CGFloat contentLength = right - left;
    CGFloat newLeft = (self.optionsContainerView.ous_width - contentLength) / 2;
    CGFloat deltaLeft = newLeft - left;
    for (UIView * subview in previousButtons)
        subview.ous_left += deltaLeft;
}

- (void)onBtnOption:(OUSPaperButton *)sender
{
    self.didTapOnRatingOrOption = YES;
    
    //Toggle current one (allow to de-select all)
    [sender ous_addFadeTransitionWithDuration:0.2];
    sender.selected = !sender.selected;
    
    //Single-select only: unselect every other buttons
    BOOL multiSelect = self.model.options_multiselect.boolValue;
    if (!multiSelect && sender.selected)
        for (UIButton * btn in self.optionsViewsArray)
            if ([btn isKindOfClass:UIButton.class])
                if ([btn isEqual:sender] == NO)
                    btn.selected = NO;
    
    //Update style of all option buttons
    for (OUSPaperButton * btn in self.optionsViewsArray)
        if ([btn isKindOfClass:OUSPaperButton.class]) {
            if (btn.selected) {
                [btn setBackgroundColor:self.model.theme.option_selected_bg_color];
                btn.layer.borderColor = self.model.theme.option_selected_border_color.CGColor;
                btn.tapCircleColor = [self.model.theme.option_selected_text_color colorWithAlphaComponent:0.25];
            }
            else {
                [btn setBackgroundColor:self.model.theme.option_bg_color];
                btn.layer.borderColor = self.model.theme.option_border_color.CGColor;
                btn.tapCircleColor = [self.model.theme.option_text_color colorWithAlphaComponent:0.25];
            }
        }
    
    //Show the UI below options
    [self relayout:YES];
}


#pragma mark - Actions

//Override
- (IBAction)onBtnClose:(UIButton *)sender
{
    //Special behaviour for iPad non-instrusive - Expanding
    if (IS_IPAD && self.isNonInstrusive && !self.isExpanded)
    {
        //Rotate close button
        OUS_WEAK_SELF
        [UIView animateWithDuration:0.25 animations:^{
            OUS_STRONG_SELF
            strongSelf.btnClose.transform = CGAffineTransformMakeRotation(M_PI_2);
        }];
        
        //Expand the form
        self.isExpanded = YES;
        [self relayout:YES];
        return;
    }
    
    //Special behaviour for iPad non-instrusive - Collapsing
    if (self.isExpanded && !self.didSubmit)
    {
        [self.activeTextField resignFirstResponder];
        [self.activeTextView resignFirstResponder];
        [self.txtComment resignFirstResponder];
        
        //Reset all flags
        self.isExpanded = NO;
        [self relayout:YES];
        
        //Rotate close button
        OUS_WEAK_SELF
        [UIView animateWithDuration:0.25 animations:^{
            OUS_STRONG_SELF
            strongSelf.btnClose.transform = CGAffineTransformMakeRotation(M_PI_4);
        }];
        return;
    }
    
    //Normal close
    [super onBtnClose:sender];
}

- (void)onTap:(UITapGestureRecognizer *)recognizer
{
    [self.activeTextField resignFirstResponder];
    [self.activeTextView resignFirstResponder];
    [self.txtComment resignFirstResponder];
    
    //No close button
    if (self.didSubmit && IS_NOT_IPAD && self.isNonInstrusive)
        [self onBtnClose:nil];
}

- (void)onNavbarTap:(UITapGestureRecognizer *)recognizer
{
    if (IS_NOT_IPAD)
        return;
    if (self.isInstrusive)
        return;
    if (self.isExpanded)
        return;
    
    //Ripple
    [self.btnNavbarBehind simulatePress:[recognizer locationInView:recognizer.view]];
    
    //Rotate close button
    OUS_WEAK_SELF
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.btnClose.transform = CGAffineTransformMakeRotation(M_PI_2);
    }];
    
    //Expand the form
    self.isExpanded = YES;
    [self relayout:YES];
}

- (void)didChangeStarRatingValue:(id)sender
{
    //Sanity check
    if (self.starRatingView.value <= 0)
        return;
    
    self.didTapOnRatingOrOption = YES;

    //Prevent double touch only when auto-submit
    OUS_PREVENT_DOUBLE_TOUCH(0.5);

    //Auto-submit, if any
    BOOL autoSubmit = [self.model hasAutoSubmitForRating:(NSInteger)self.starRatingView.value];
    if (autoSubmit) {
        [self submitForm:YES];              //Auto submit, skip some form validations
        return;
    }

    [self constructOptionsButtons:YES];
    [self relayout:YES];
}

- (void)didChangeNumberRatingValue:(id)sender
{
    //Sanity check
    if (self.numberRatingView.value <= 0)
        return;
    
    self.didTapOnRatingOrOption = YES;
    
    [self constructOptionsButtons:YES];
    [self relayout:YES];
    
    //Auto-submit, if any
    BOOL autoSubmit = [self.model hasAutoSubmitForRating:(NSInteger)self.numberRatingView.value];
    if (!autoSubmit)
        return;
    
    //Prevent double touch only when auto-submit
    OUS_PREVENT_DOUBLE_TOUCH(0.5);
    
    //Auto submit, skip some form validations
    [self submitForm:YES];
}


#pragma mark - Submit Actions

- (IBAction)onBtnAction:(id)sender
{
    OUS_PREVENT_DOUBLE_TOUCH(0.5);
    
    [self.activeTextField resignFirstResponder];
    [self.activeTextView resignFirstResponder];
    [self.txtComment resignFirstResponder];

    //First time, perform a normal /submit
    if (!self.didSubmit) {
        [self submitForm:NO];
        return;
    }
    
    //Check for valid store review URL
    //Redirect to app store review only, next UI logics are handle below
    BOOL hasManualStoreReview = self.model.auto_store_review.boolValue == NO && self.model.ios_review_url.ous_isHttpUrl;
    if (hasManualStoreReview)
        [self goToAppStoreReviewOnly];
    
    //Extra model logics
    //We're showing intermediate UI, clear it and re-apply same UI (show normal Success UI)
    if (self.original_model != nil) {
        self.model = self.original_model;       //switch back to original model
        self.model.extra_form = nil;            //this is important
        self.original_model = nil;              //just to be sure
        [self handleSubmissionResponse:self.model];
        return;
    }
    
    //Simple close logic
    [self onBtnClose:self.btnClose];
}

- (NSMutableArray *)selectedOptionSlugs
{
    //No options if entire area is hidden
    if (self.optionsContainerView.ous_visible == NO || self.optionsContainerView.alpha <= 0)
        return nil;
    
    NSMutableArray * selectedOptions = [NSMutableArray array];
    for (UIButton * btn in self.optionsContainerView.subviews)
    {
        if (btn.alpha <= 0 || btn.ous_visible == NO)
            continue;
        if (btn.selected == NO)
            continue;
        
        OUSOption * option = [btn ous_getTagObject];
        if ([option isKindOfClass:OUSOption.class] == NO)
            continue;
        
        NSString * slug = option.slug;
        if (slug == nil || [slug isKindOfClass:NSString.class] == NO)
            continue;
        if (slug.ous_trim.length <= 0)
            continue;
        
        [selectedOptions addObject:slug];
    }
    
    return selectedOptions;
}

- (BOOL)validateForm:(BOOL)isAutoSubmit
{
    //Validate rating & options
    if (self.model.hasRatingOrOption)
        if (!self.didTapOnRatingOrOption) {
            [self.starRatingViewPlaceholder ous_shakeX];
            [self.numberRatingViewPlaceholder ous_shakeX];
            [self.optionsContainerView ous_shakeX];
            [self.btnSubmit ous_shakeX];
            return NO;
        }

    //This is extra check, not really neccessary, already covered by 1st check
    //Validate star rating, if any
    if (self.model.show_rating_star.boolValue)
        if (self.starRatingView.value <= 0) {
            [self.starRatingViewPlaceholder ous_shakeX];
            [self.numberRatingViewPlaceholder ous_shakeX];
            [self.btnSubmit ous_shakeX];
            return NO;
        }
    
    //This is extra check, not really neccessary, already covered by 1st check
    //Validate number rating, if any
    if (self.model.show_rating_number.boolValue)
        if (self.numberRatingView.value <= 0) {
            [self.starRatingViewPlaceholder ous_shakeX];
            [self.numberRatingViewPlaceholder ous_shakeX];
            [self.btnSubmit ous_shakeX];
            return NO;
        }
    
    //Validate comment area, if visible
    if (self.model.comment_mandatory.boolValue)
        if (self.textAreaContainerView.alpha > 0)
            if (self.txtComment.text.ous_trim.length < 1) {
                [self.textAreaContainerView ous_shakeX];
                [self.btnSubmit ous_shakeX];
                return NO;
            }
    
    return YES;
}

- (void)submitForm:(BOOL)isAutoSubmit
{
    //Validate
    BOOL isValid = [self validateForm:(BOOL)isAutoSubmit];
    if (!isValid)
        return;
    
    //Dim the button
    self.btnSubmit.alpha = 0.5;
    self.btnSubmitContainer.userInteractionEnabled = NO;
    
    //Data to submit
    NSInteger rating = -1;
    if (self.model.show_rating_star.boolValue)          rating = self.starRatingView.value;
    else if (self.model.show_rating_number.boolValue)   rating = self.numberRatingView.value;
    
    //Don't submit comment if it's being hidden
    NSString * comment = [self.txtComment.text ous_trim];
    if (self.textAreaContainerView.ous_visible == NO || self.textAreaContainerView.alpha <= 0)
        comment = nil;
    
    //Just to be sure
    self.didSubmit = NO;
    
    //API call
    __weak typeof (self) weakSelf = self;
    [UserSightAPI submitRating:rating
                        formSlug:self.model.slug
                        eventTag:self.model.event_tag
                       requestId:self.model.request_id
                         options:self.selectedOptionSlugs
                         comment:comment
                           debug:self.model.debug
                      autoSubmit:isAutoSubmit
                      completion:^(OUSForm * _Nullable model, NSError * _Nullable error) {
        
        //Error
        if (error)
            [weakSelf performSelectorOnMainThread:@selector(handleSubmissionCriticalError:) withObject:error waitUntilDone:NO];
        
        //Success
        else
            [weakSelf performSelectorOnMainThread:@selector(handleSubmissionResponse:) withObject:model waitUntilDone:NO];
    }];
    
    //Save pending external link, if any
    //To be opened only after /submit API call
    BOOL hasExternalLink = [self.model.url ous_isHttpUrl];
    if (hasExternalLink) {
        NSURL * url = [NSURL URLWithString:self.model.url];
        if (url != nil && [[UIApplication sharedApplication] canOpenURL:url])
            self.pendingExternalUrl = url;
    }
}



#pragma mark - Post Submission

- (void)openExternalUrlIfAny
{
    //Pending external URL, if any
    if ([self.pendingExternalUrl isKindOfClass:NSURL.class] == NO)
        return;

    //Clear pending variable first
    NSURL * url = self.pendingExternalUrl;
    self.pendingExternalUrl = nil;
    
    //Open it
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}

//Critical network error
- (void)handleSubmissionCriticalError:(NSError *)error
{
    //Pending external URL, if any
    [self openExternalUrlIfAny];
    
    //Debug info
    if (self.model.debug)
        NSLog(@"FeedbackSDK Error: %@", error);
    
    //Restore Submit/Close button
    [self.btnSubmit ous_fadeInWithDuration:0.25];
    self.btnSubmitContainer.userInteractionEnabled = YES;
    
    //Just to be sure
    self.didSubmit = NO;
}

//API success, may contains a business logic failure
- (void)handleSubmissionResponse:(OUSForm *)model
{
    //Pending external URL, if any
    [self openExternalUrlIfAny];
    
    //Error
    if (model.isError) {
        NSLog(@"FeedbackSDK Error: %@", model.error);
        [self showAlertWithTitle:@"FeedbackSDK Error" message:model.error];
        [self onBtnClose:nil];
        return;
    }
    
    //Check for extra model & store the original model
    if (model.extra_form != nil) {
        self.original_model = model;
        self.model = model.extra_form;
    }
    else {
        self.original_model = nil;
        self.model = model;
    }

    //Update Flags
    self.didSubmit = YES;

    //Clean up selected options
    [self constructOptionsButtons:YES];

    //Restore Submit button
    [self.btnSubmit ous_fadeInWithDuration:0.25];
    self.btnSubmitContainer.userInteractionEnabled = YES;
        
    //Automatically show store review (and show success UI)
    BOOL autoShowStoreReview = self.model.auto_store_review.boolValue && self.model.ios_review_url.ous_isHttpUrl;
    if (autoShowStoreReview) {
        [self autoAppStoreReviewAndShowSuccessUI];
        return;
    }
    
    //Normal flow
    [self showSuccessUI];
}

//Fully success
- (void)showSuccessUI
{
    //Auto close on iPhone non-instrusive
    if (IS_NOT_IPAD && self.isNonInstrusive)
    {
        [self.btnClose removeFromSuperview];
        self.btnClose = nil;
        [self.btnSubmitContainer removeFromSuperview];
        self.btnSubmitContainer = nil;
        [self performSelector:@selector(onBtnClose:) withObject:nil afterDelay:2];
    }
    
    [self relayout:YES];
}

- (void)autoAppStoreReviewAndShowSuccessUI
{
    //This popup can only be shown only a few times per year (server side logic)
    [self setDidPromptInAppReview];
    [SKStoreReviewController requestReview];
    
    //Show success UI automatically
    [self showSuccessUI];
}

- (void)goToAppStoreReviewOnly
{
    //Sanity check and fallback
    NSURL * url = [NSURL URLWithString:self.model.ios_review_url];
    if (url == nil)
        return;
    
    //Sanity check
    BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:url];
    if (!canOpen)
        return;

    //Redirect to store
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}

@end
