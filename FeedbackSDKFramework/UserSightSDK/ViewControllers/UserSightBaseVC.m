//
//  OUSFeedbackBaseVC.m
//

#import "UserSightBaseVC.h"
#import "OUSPassThroughView.h"
#import "UIView+OUSAdditions.h"
#import "NSNumber+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"
#import "UserSightAPI.h"

@interface UserSightBaseVC ()
@end

@implementation UserSightBaseVC

#pragma mark - Init

- (id)init
{
    self = [super init];
    if (self == nil)
        return nil;
    
    [self setup];
    
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    self.completionBlock = nil;

    self.activeTextField = nil;
    self.activeTextView = nil;
    
    self.original_model = nil;
    self.model = nil;
}

- (UIViewController *)initWithNib
{
    //Retrieve the current bundle used by our Pod (not the main bundle)
    NSBundle * bundle = [NSBundle ous_podBundleWithClass:self.classForCoder];
    NSString * className = NSStringFromClass(self.class);
    
    self = [self initWithNibName:className bundle:bundle];
    if (self == nil)
        return nil;
    
    [self setup];
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //Retrieve the current bundle used by our Pod (not the main bundle)
    if (nibBundleOrNil == nil)
        nibBundleOrNil = [NSBundle ous_podBundleWithClass:self.classForCoder];
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self == nil)
        return nil;
    
    [self setup];
    
    return self;
}

//Basic UI setup without data model
- (void)setup
{
    self.shouldHandleKeyboardNotification = YES;
    
    self.restorationIdentifier = NSStringFromClass(self.class);
    self.restorationClass = self.class;
    
    //Disable iOS13 sheet presentation style
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    //Allow touch pass thru
    self.popupContainerView.tag = OUS_PASS_THROUGH_VIEW_TAGS + 1;
}


#pragma mark - Helpers

- (void)clearDelegate
{
    self.delegate = nil;
}

- (void)applyRoundedCorners
{
    //just to be sure
    self.popupContainerView.backgroundColor = UIColor.clearColor;
    
    [self.navbar ous_roundCorners:UIRectCornerTopLeft|UIRectCornerTopRight
                       withRadius:UI_NON_INSTRUSIVE_ROUND_CORNERS];
}

- (BOOL)isInstrusive
{
    return self.instrusive_style == YES;
}

- (BOOL)isNonInstrusive
{
    return self.instrusive_style == NO;
}


#pragma mark -

//Default slide in transition
- (void)introAnimation:(void (^ __nullable)(BOOL finished))completion
{
    //Hide background color first
    UIColor * backgroundColor = self.view.backgroundColor;
    self.view.backgroundColor = UIColor.clearColor;
    
    //Hide away initially
    self.popupContainerView.ous_top = self.popupContainerView.superview.ous_height;
    
    //Slide up the popup container view
    OUS_WEAK_SELF
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        OUS_STRONG_SELF
        
        if (strongSelf.isInstrusive)
            strongSelf.popupContainerView.ous_centerY = strongSelf.popupContainerView.superview.ous_contentCenter.y;
        else
            strongSelf.popupContainerView.ous_bottom = strongSelf.popupContainerView.superview.ous_height;
        
        strongSelf.view.backgroundColor = backgroundColor;
        
    } completion:completion];
}

//Default slide out transition
- (void)outroAnimation:(void (^ __nullable)(BOOL finished))completion
{
    //Slide down the popup container view
    OUS_WEAK_SELF
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        OUS_STRONG_SELF
        
        strongSelf.popupContainerView.ous_top = strongSelf.popupContainerView.superview.ous_height;
        
        //Fade out background color
        strongSelf.view.backgroundColor = UIColor.clearColor;
        
    } completion:completion];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:title
                                                                      message:message
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertVC addAction:defaultAction];
    
    UIWindow * alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];

    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        alertWindow.tintColor = delegate.window.tintColor;
    }

    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow * topWindow = [UIApplication sharedApplication].windows.lastObject;
    alertWindow.windowLevel = topWindow.windowLevel + 1;

    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alertVC animated:YES completion:nil];
}


#pragma mark - Lifecycle

//Override
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Latch instrustive/non-instrusive style value
    if (self.model != nil)
        self.instrusive_style = self.model.instrusive_style.boolValue;
    
    //Extra bottom padding for devices without physical Home button
    if (@available(iOS 11.0, *)) {
        UIWindow * window = UIApplication.sharedApplication.keyWindow;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        self.bottomBarOffset = bottomPadding / 2;
    }
    
    BOOL needExtraTopPadding = (self.modalPresentationStyle == UIModalPresentationFullScreen);
    
    //Handle Navbar for devices with rabbit ears
    if (needExtraTopPadding) {
        if (@available(iOS 11.0, *)) {
            UIWindow * window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
            self.statusBarOffset = MAX(topPadding, statusBarHeight);
        }
        else {
            CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
            self.statusBarOffset = statusBarHeight;
            if (self.navbarTopSafeAreaConstraint != nil)
                self.navbarTopSafeAreaConstraint.constant = -self.statusBarOffset;
        }
    }
    
    //Convert "X" image to template
    UIImage * btnCloseImage = [self.btnClose imageForState:UIControlStateNormal];
    btnCloseImage = [btnCloseImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnClose setImage:btnCloseImage forState:UIControlStateNormal];
    
    //Shadow for the entire popup container view
    //Note: top rounded corners must be applied to navbar only
    self.popupContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.popupContainerView.layer.shadowRadius = 16;
    self.popupContainerView.layer.shadowOffset = CGSizeMake(0, 4);
    self.popupContainerView.layer.shadowOpacity = 0.1;
    self.popupContainerView.layer.masksToBounds = NO;
    self.popupContainerView.clipsToBounds = NO;
    
    //Internal sanity checks
    if ([self.view isKindOfClass:OUSPassThroughView.class] == NO) {
        NSLog(@"!!! FeedbackVC.view is not OUSPassThroughView class (actual class: %@)", NSStringFromClass(self.view.class));
        NSLog(@"!!! FeedbackVC.view is not OUSPassThroughView class (actual class: %@)", NSStringFromClass(self.view.class));
        NSLog(@"!!! FeedbackVC.view is not OUSPassThroughView class (actual class: %@)", NSStringFromClass(self.view.class));
        NSLog(@"!!! FeedbackVC.view is not OUSPassThroughView class (actual class: %@)", NSStringFromClass(self.view.class));
    }
}

//Override
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Keyboard notification
    if (self.shouldHandleKeyboardNotification)
        [self registerForKeyboardNotifications];
    
    //Workaround for top rounded corners only
    if (self.isNonInstrusive)
        [self applyRoundedCorners];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self unregisterForKeyboardNotifications];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    NSLog(@"%@ didReceiveMemoryWarning", NSStringFromClass(self.class));
}


#pragma mark - Device orientation support

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    //Doesn't support landscape on iPhone
    if (IS_NOT_IPAD)
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    
    //iPad
    return UIInterfaceOrientationMaskLandscape | UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}


#pragma mark - Actions

- (void)forceDismiss:(BOOL)animated
{
    //No animation
    if (!animated) {
        //Remove self from window/superview
        [self.view removeFromSuperview];
        self.view.hidden = YES;
        [self selfDidDismissed];
        return;
    }
    
    //With animation
    OUS_WEAK_SELF
    [self outroAnimation:^(BOOL finished) {
        OUS_STRONG_SELF
        
        //Remove self from window/superview
        [strongSelf.view ous_fadeOutWithDuration:0.25 andRemoveFromSuperView:YES completion:^(BOOL finished) {
            [strongSelf selfDidDismissed];
        }];
    }];
}

- (IBAction)onBtnClose:(id)sender
{
    //Use Back button to dismiss keyboard
    if (self.activeTextField != nil && [self.activeTextField isFirstResponder]) {
        [self.activeTextField resignFirstResponder];
        return;
    }
    if (self.activeTextView != nil && [self.activeTextView isFirstResponder]) {
        [self.activeTextView resignFirstResponder];
        return;
    }
    
    OUS_WEAK_SELF
    [self outroAnimation:^(BOOL finished) {
        OUS_STRONG_SELF
        
        //Remove self from window/superview
        [strongSelf.view ous_fadeOutWithDuration:0.25 andRemoveFromSuperView:YES completion:^(BOOL finished) {
            [strongSelf selfDidDismissed];
        }];
    }];
}

- (void)selfDidDismissed
{
    //Callback to host application, if any
    //Did show = YES
    if (self.completionBlock)
        self.completionBlock(YES);
    self.completionBlock = nil;
    
    //Internal callback to UserSightSDK instance for cleaning up
    if ([self.delegate respondsToSelector:@selector(internal_formDidDismissed:)])
        [self.delegate internal_formDidDismissed:self];
    self.delegate = nil;
}


#pragma mark - Keyboard

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardDidHideNotification:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - Keyboard events

/*
 * This is needed because iOS always return keyboard size in landscape orientation
 */
- (CGFloat)getCorrectKeyboardHeight:(CGSize)originalSize
{
    return MIN(originalSize.height, originalSize.width);
}

/*
 * This is needed because iOS always return keyboard size in landscape orientation
 */
- (CGFloat)getCorrectKeyboardWidth:(CGSize)originalSize
{
    return MAX(originalSize.height, originalSize.width);
}

- (void)onKeyboardWillShowNotification:(NSNotification *)sender
{
    /*
     CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
     CGFloat kbHeight = [self getCorrectKeyboardHeight:kbSize];
     
     NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
     UIViewAnimationOptions animationCurve = [[[sender userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
     */
    
}

- (void)onKeyboardWillHideNotification:(NSNotification *)sender
{
    /*
     NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
     UIViewAnimationOptions animationCurve = [[[sender userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
     */
}

- (void)onKeyboardDidHideNotification:(NSNotification *)sender
{
    /*
     NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
     UIViewAnimationOptions animationCurve = [[[sender userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
     */
}


#pragma mark - In-App System Native 5-star rating

- (void)setDidPromptInAppReview
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"H2lUDI5Y5eq832Hzl5ns"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)didPromptInAppReview
{
    NSDate * didShowDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"H2lUDI5Y5eq832Hzl5ns"];
    return didShowDate != nil;
}

@end
