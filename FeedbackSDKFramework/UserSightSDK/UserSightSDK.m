//
//  UserSightSDK.m
//

#include <stdlib.h>
#import <CoreText/CoreText.h>
#import "UserSightSDK.h"
#import "UserSightAPI.h"
#import "UserSightViewController.h"
#import "NSString+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"
#import "UIFont+OUSAdditions.h"
#import "OUSForm.h"

#define USERSIGHT_SDK_VERSION                   @"0.2.0"
#define USERSIGHT_SDK_ERROR_TITLE               @"UserSightSDK Error"
#define USERSIGHT_SDK_DEFAULT_SLUG              @"satisfaction-1"

@interface UserSightSDK () <OUSFeedbackInternalDelegate>
@property (nonatomic, strong) UserSightBaseVC * currentVC;
@end

@implementation UserSightSDK

static NSNumber * isInitialized = nil;
static BOOL alwaysOnTop = YES;

+ (UserSightSDK *)sharedInstance
{
    static dispatch_once_t pred;
    static id __singleton = nil;
    dispatch_once(&pred, ^{
        __singleton = [[self alloc] init];
    });
    return __singleton;
}


#pragma mark - Basic Interfaces

+ (NSString *)version
{
    return USERSIGHT_SDK_VERSION;
}

+ (BOOL)isShowing
{
    return [self sharedInstance].currentVC != nil;
}

+ (void)forceDismiss:(BOOL)animated
{
    //internal_formDidDismissed delegate will handle the clean up

    //Make sure this is running on main thread
    if ([NSThread isMainThread]) {
        [[self sharedInstance].currentVC forceDismiss:animated];
        return;
    }
    
    //Switch to main thread
    OUS_WEAK_SELF
    dispatch_async(dispatch_get_main_queue(), ^{
        OUS_STRONG_SELF
        [[strongSelf sharedInstance].currentVC forceDismiss:animated];
    });
}


#pragma mark - Initialization

+ (void)initWithAppSecret:(NSString *)appSecret application:(UIApplication *)application
{
    if (!appSecret)
        appSecret = @"";
    
    //Reset
    [UserSightAPI setLanguage:nil];
    [UserSightAPI setUserId:nil];
    
    //Store app secret
    [UserSightAPI setAppSecret:appSecret];
        
    isInitialized = @YES;
    
    //Preload fonts 15-30s later
    int rand = 15 + arc4random_uniform(15);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(rand * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self preload];
    });
}

+ (void)setAppSecret:(NSString *)appSecret
{
    [UserSightAPI setAppSecret:appSecret];
}

+ (void)setUserId:(NSString *)userId
{
    [UserSightAPI setUserId:userId];
}

+ (NSString *)language
{
    return [UserSightAPI lang];
}

+ (void)setLanguage:(NSString *)lang
{
    [UserSightAPI setLanguage:lang];
}

+ (void)alwaysOnTop:(BOOL)enabled
{
    alwaysOnTop = enabled;
}


#pragma mark - Metadata

+ (void)clearMetadata
{
    [UserSightAPI clearMetadata];
}

+ (void)setMetadata:(NSString * _Nullable)value
{
    [UserSightAPI setMetadata:value];
}

+ (void)setCustomMetadata:(NSString * _Nullable)key value:(NSString * _Nullable)value
{
    [UserSightAPI setCustomMetadata:key value:value];
}


#pragma mark - Presentation

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:nil completion:nil];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                           completion:(UserSightSDKCompletionBlock _Nullable)completion
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:nil completion:completion];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:formSlug completion:nil];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                             formSlug:(NSString * _Nullable)formSlug
                           completion:(UserSightSDKCompletionBlock _Nullable)completion
{
    OUS_PREVENT_DOUBLE_TOUCH(1);
    
    //Checking whether the SDK has been initialized before
    BOOL isInitialized = [self checkInitialized];
    if (!isInitialized) {
        [self showAlertWithTitle:USERSIGHT_SDK_ERROR_TITLE
                         message:@"FeedbackSDK has not been initialized with an App Secret (Error code: 2814)!"];
        if (completion)
            completion(NO);
        return;
    }
    
    //Checking whether the SDK is being shown
    BOOL isShowing = [self isShowing];
    if (isShowing) {
        if (completion)
            completion(NO);
        return;
    }
        
    //Optional: override default formSlug
    if (formSlug == nil || formSlug.ous_trim.length <= 0)
        formSlug = USERSIGHT_SDK_DEFAULT_SLUG;

    //Call API to request for display
    OUS_WEAK_SELF
    [UserSightAPI requestForm:formSlug
                     eventTag:eventTag
                        debug:YES
                   completion:^(OUSForm * _Nullable model, NSError * _Nullable error) {
        OUS_STRONG_SELF
        
        //Invalid model, possible wrong format or network error
        //Don't show error alert
        if (model == nil) {
            NSLog(@"No form to be displayed in debugFeedbackFormWithEventTag: %@", error);
            if (completion)
                completion(NO);
            return;
        }
        
        //Business logic error
        if (model.isError) {
            [self showAlertWithTitle:USERSIGHT_SDK_ERROR_TITLE message:model.error];
            return;
        }
        
        //No form to show, on purpose
        if (model.hasValidForm == NO) {
            NSLog(@"No form to be displayed in debugFeedbackFormWithEventTag");
            if (completion)
                completion(NO);
        }
        
        //Show it in UI
        [strongSelf showFormModel:model completion:completion];
    }];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:nil completion:nil];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                          completion:(UserSightSDKCompletionBlock _Nullable)completion
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:nil completion:completion];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:formSlug completion:nil];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                            formSlug:(NSString * _Nullable)formSlug
                          completion:(UserSightSDKCompletionBlock _Nullable)completion
{
    OUS_PREVENT_DOUBLE_TOUCH(1);
    
    //Checking whether the SDK has been initialized before
    BOOL isInitialized = [self checkInitialized];
    if (!isInitialized) {
        if (completion)
            completion(NO);
        return;
    }
    
    //Checking whether the SDK is being shown
    BOOL isShowing = [self isShowing];
    if (isShowing) {
        if (completion)
            completion(NO);
        return;
    }
        
    //Optional: override default formSlug
    if (formSlug == nil || formSlug.ous_trim.length <= 0)
        formSlug = USERSIGHT_SDK_DEFAULT_SLUG;
    
    //Call API to request for display
    OUS_WEAK_SELF
    [UserSightAPI requestForm:formSlug
                     eventTag:eventTag
                        debug:NO
                   completion:^(OUSForm * _Nullable model, NSError * _Nullable error) {
        OUS_STRONG_SELF

        //Invalid model, possible wrong format or network error
        //Don't show error alert
        if (model == nil) {
            if (error)
                NSLog(@"No form to be displayed in showFeedbackFormWithEventTag: %@", error);
            if (completion)
                completion(NO);
            return;
        }
        
        //Critical error, possibly due to wrong config
        if (model.isError) {
            NSLog(@"No form to be displayed in showFeedbackFormWithEventTag: %@", model.error);
            return;
        }
        
        //No form to show, on purpose
        if (model.hasValidForm == NO) {
            NSLog(@"No form to be displayed in showFeedbackFormWithEventTag");
            if (completion)
                completion(NO);
            return;
        }
        
        //Show it in UI
        [strongSelf showFormModel:model completion:completion];
    }];
}


#pragma mark - Preload

+ (void)preload
{
    OUS_PREVENT_DOUBLE_TOUCH(1);
    
    //Checking whether the SDK has been initialized before
    BOOL isInitialized = [self checkInitialized];
    if (!isInitialized)
        return;
    
    //Call API to request for display
    [UserSightAPI preload:YES
               completion:^(OUSPreload * _Nullable model, NSError * _Nullable error) {
        
        //Invalid model, possible wrong format or network error
        //Don't show error alert
        if (model == nil)
            return;
        
        //Business logic error
        if (model.isError) {
            [self showAlertWithTitle:USERSIGHT_SDK_ERROR_TITLE message:model.error];
            return;
        }
        
        //Preload fonts
        if (model.fonts)
            for (OUSFont * font in model.fonts)
                if ([font isKindOfClass:OUSFont.class])
                    [UIFont ous_loadFontName:font.name filename:font.file url:font.file_url];
    }];
}


#pragma mark - Helpers

+ (BOOL)checkInitialized
{
    NSString * appSecret = [UserSightAPI appSecret];
    if (appSecret.ous_trim.length < 8)
    {
        NSLog(@"[FeedbackSDK] FeedbackSDK has not been initialized with an invalid App Secret (Error code: 2815)");
        return NO;
    }
    
    //Automatically initialize the SDK
    if (NO == isInitialized.boolValue)
    {
        NSLog(@"[FeedbackSDK] Please initialize FeedbackSDK with initWithAppSecret:application: function (Error code: 2816)");
        return NO;
    }
    
    return YES;
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:title
                                                                      message:message
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertVC addAction:defaultAction];
    
    UIWindow * alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];

    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        alertWindow.tintColor = delegate.window.tintColor;
    }

    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow * topWindow = [UIApplication sharedApplication].windows.lastObject;
    alertWindow.windowLevel = topWindow.windowLevel + 1;

    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alertVC animated:YES completion:nil];
}

+ (UIWindow *)frontWindow
{
#if defined(SV_APP_EXTENSIONS)
    return nil;
#endif
    
    NSEnumerator * frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
    for (UIWindow * window in frontToBackWindows)
    {
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        BOOL windowLevelSupported = window.windowLevel >= UIWindowLevelNormal && window.windowLevel <= UIWindowLevelNormal;        //UIWindowLevelAlert
        
        if (windowOnMainScreen && windowIsVisible && windowLevelSupported)
            return window;
    }
    return nil;
}

+ (void)showFormModel:(OUSForm *)model completion:(UserSightSDKCompletionBlock)completion
{
    //Just to be sure again
    //No valid form to show
    if (model.hasValidForm == NO) {
        NSLog(@"No valid form to be displayed in showFormModel");
        return;
    }
    
    //Make sure this is running on main thread
    if ([NSThread isMainThread]) {
        [[self sharedInstance] internal_showFormModel:model completion:completion];
        return;
    }
    
    //Switch to main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self sharedInstance] internal_showFormModel:model completion:completion];
    });
}

- (void)internal_showFormModel:(OUSForm *)model completion:(UserSightSDKCompletionBlock)completion
{
    //Already showing
    if (self.currentVC != nil)
        return;
       
    UserSightViewController * vc = [[UserSightViewController alloc] initWithNib];
    vc.completionBlock = completion;
    vc.delegate = self;
    vc.model = model;

    //Keep a strong reference
    self.currentVC = vc;
    
    //Floating subview in main window
    UIView * windowView = [[self class] frontWindow];
    [windowView addSubview:vc.view];
    [windowView bringSubviewToFront:vc.view];
    vc.view.frame = windowView.bounds;
    vc.view.autoresizingMask = AUTORESIZING_MASK_WIDTH_HEIGHT;
    [vc introAnimation:nil];

    //KVO
    [windowView.layer addObserver:self forKeyPath:@"sublayers" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (alwaysOnTop == NO)
        return;
    
    if (self.currentVC == nil || self.currentVC.view == nil)
        return;
    
    [self.currentVC.view.superview bringSubviewToFront:self.currentVC.view];
}


#pragma mark - OUSFeedbackInternalDelegate

- (void)internal_formDidDismissed:(UIViewController * _Nonnull)viewController
{
    //Release reference
    self.currentVC = nil;
    
    //Remove KVO observer
    @try{
        [[[self class] frontWindow] removeObserver:self forKeyPath:@"sublayers" context:NULL];
    }
    @catch(id anException){
        //Do nothing here
    }
}

@end
