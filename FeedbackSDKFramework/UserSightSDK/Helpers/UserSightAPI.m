//
//  OUSFeedbackAPI.m
//  UserSightSDK
//

#import <stdlib.h>
#import <sys/utsname.h>
#import <Security/Security.h>
#import "UserSightAPI.h"
#import "UserSightSDK.h"
#import "OUSDeviceUUID.h"
#import "NSDate+OUSAdditions.h"
#import "NSString+OUSAdditions.h"
#import "NSArray+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"

#define IS_IPAD                              (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define USERSIGHT_SDK_INSTALL_DATE           @"SZCEVo0dYb2nH5PYuYhz"
#define USERSIGHT_SDK_APP_SECRET             @"aB7hgJpv1k2HYXy29QGN"
#define USERSIGHT_SDK_APP_USER_ID            @"rXsqP1MI7xcs5GAZoPiN"
#define USERSIGHT_SDK_LANGUAGE               @"APVbmYNbXQJexs4XIbWG"
#define USERSIGHT_SDK_METADATA               @"bQgJyNbXAPVNbXAPVXaB"
#define USERSIGHT_SDK_METADATA_DEFAULT_KEY   @"metadata"                    //default 'key' for metadata for initial implementation
#define USERSIGHT_SDK_API_PRELOAD            @"JyrS9vA9Va4vI7HOhMgK"        //do not change this
#define USERSIGHT_SDK_API_REQUEST            @"8vs2qwFD1rlSFWIa13Wc"        //do not change this
#define USERSIGHT_SDK_API_SUBMIT             @"LXvcFSTTOqxxmJX9qQar"        //do not change this

@implementation UserSightAPI

+ (UserSightAPI *)sharedInstance
{
    static dispatch_once_t pred;
    static id __singleton = nil;
    dispatch_once(&pred, ^{
        __singleton = [[self alloc] init];
    });
    return __singleton;
}

+ (NSString *)appSecret
{
    NSString * value = [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_APP_SECRET];
    if (!value)
        value = @"";
    return value;
}

+ (void)setAppSecret:(NSString *)appSecret
{
    [[NSUserDefaults standardUserDefaults] setObject:appSecret forKey:USERSIGHT_SDK_APP_SECRET];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_APP_USER_ID];
}

+ (void)setUserId:(NSString *)userId
{
    if (!userId)
        userId = @"";
    
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:USERSIGHT_SDK_APP_USER_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lang
{
    NSString * lang = [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_LANGUAGE];
    if (lang == nil || lang.length < 2)     //allow language codes such as "zh_tw"
        lang = @"en";
    
    return lang;
}

+ (void)setLanguage:(NSString *)lang
{
    [[NSUserDefaults standardUserDefaults] setObject:lang forKey:USERSIGHT_SDK_LANGUAGE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Metadata

//private
+ (NSMutableDictionary *)metadata
{
    NSMutableDictionary * mutableDict = nil;
    
    //Initialize storage dictionary
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_METADATA];
    if ([dict isKindOfClass:NSDictionary.class] && dict.count > 0)
        mutableDict = [dict mutableCopy];
    else
        mutableDict = [NSMutableDictionary dictionary];
    
    return mutableDict;
}

+ (void)clearMetadata
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERSIGHT_SDK_METADATA];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setMetadata:(NSString * _Nullable)value
{
    NSMutableDictionary * mutableDict = [self metadata];
    if (value == nil)
        [mutableDict removeObjectForKey:USERSIGHT_SDK_METADATA_DEFAULT_KEY];
    else
        [mutableDict setObject:value forKey:USERSIGHT_SDK_METADATA_DEFAULT_KEY];
    
    [[NSUserDefaults standardUserDefaults] setObject:mutableDict forKey:USERSIGHT_SDK_METADATA];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setCustomMetadata:(NSString * _Nullable)key value:(NSString * _Nullable)value
{
    NSMutableDictionary * mutableDict = [self metadata];
    if (value == nil)
        [mutableDict removeObjectForKey:key];
    else
        [mutableDict setObject:value forKey:key];
    
    [[NSUserDefaults standardUserDefaults] setObject:mutableDict forKey:USERSIGHT_SDK_METADATA];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - High level APIs

+ (void)preload:(BOOL)debug
     completion:(void (^)(OUSPreload * _Nullable model, NSError * _Nullable error))completionHandler
{
    //Network request
    [UserSightAPI requestApiPath:USERSIGHT_SDK_API_PRELOAD
                      parameters:nil
                    attemptCount:0
                      completion:^(NSData * _Nullable data, NSDictionary * _Nullable json, NSError * _Nullable error) {
        //Error scenario
        if (error != nil) {
            if (completionHandler)
                completionHandler(nil, error);
            return;
        }
        
        //Convert to data model classes
        OUSPreload * model = nil;
        if ([json isKindOfClass:NSDictionary.class])
            model = [[OUSPreload alloc] initWithDictionary:json];
        
        //Make sure these are copied and retained
        model.debug = debug;
        
        if (completionHandler)
            completionHandler(model, error);
    }];
}

+ (void)requestForm:(NSString * _Nullable)slug
           eventTag:(NSString * _Nullable)eventTag
              debug:(BOOL)debug
         completion:(void (^)(OUSForm * _Nullable model, NSError * _Nullable error))completionHandler
{
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:@(debug) forKey:@"de"@"bu"@"g"];
    if (slug)           [params setObject:slug forKey:@"s"@"lu"@"g"];
    if (eventTag)       [params setObject:eventTag forKey:@"ev"@"ent"@"_ta"@"g"];
    
    //App install/reinstall timestamp
    NSNumber * installTimestamp = [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_INSTALL_DATE];
    if (installTimestamp == nil || installTimestamp.longLongValue <= 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970]) forKey:USERSIGHT_SDK_INSTALL_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        installTimestamp = @(0);
    }
    [params setObject:installTimestamp forKey:@"inst"@"all_t"@"ime"@"sta"@"mp"];
    
    //Network request
    [UserSightAPI requestApiPath:USERSIGHT_SDK_API_REQUEST
                      parameters:params
                    attemptCount:0
                      completion:^(NSData * _Nullable data, NSDictionary * _Nullable json, NSError * _Nullable error) {
        //Error scenario
        if (error != nil) {
            if (completionHandler)
                completionHandler(nil, error);
            return;
        }
        //No form to show
        if (json == nil || json.count < 2) {
            if (completionHandler)
                completionHandler(nil, nil);
            return;
        }
        
        //Convert to data model classes
        OUSForm * model = nil;
        if ([json isKindOfClass:NSDictionary.class])
            model = [[OUSForm alloc] initWithDictionary:json];
        
        //Make sure these are copied and retained
        model.slug = slug;
        model.event_tag = eventTag;
        model.debug = debug;
        
        if (completionHandler)
            completionHandler(model, error);
    }];
}

+ (void)submitRating:(NSInteger)rating
            formSlug:(NSString * _Nullable)slug
            eventTag:(NSString * _Nullable)eventTag
           requestId:(id _Nullable)requestId
             options:(NSArray * _Nullable)optionSlugs
             comment:(NSString * _Nullable)comment
               debug:(BOOL)debug
          autoSubmit:(BOOL)isAutoSubmit
          completion:(void (^)(OUSForm * _Nullable model, NSError * _Nullable error))completionHandler
{
    NSString * optionsString = nil;
    if (optionSlugs.count > 0)
        optionsString = [optionSlugs componentsJoinedByString:@","];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:@(isAutoSubmit) forKey:@"au"@"t"@"o"];
    [params setObject:@(debug) forKey:@"de"@"b"@"ug"];
    [params setObject:@(IS_IPAD) forKey:@"ta"@"bl"@"et"];
    if (rating >= 0)    [params setObject:@(rating) forKey:@"ra"@"tin"@"g"];
    if (eventTag)       [params setObject:eventTag forKey:@"ev"@"en"@"t_t"@"ag"];
    if (slug)           [params setObject:slug forKey:@"s"@"lug"];
    if (comment)        [params setObject:comment forKey:@"fre"@"e_te"@"xt"];
    if (requestId)      [params setObject:requestId forKey:@"req"@"uest_i"@"d"];
    if (optionsString)  [params setObject:optionsString forKey:@"sel"@"ect"@"ed"@"_opt"@"io"@"ns"];
    
    //App install/reinstall timestamp
    NSNumber * installTimestamp = [[NSUserDefaults standardUserDefaults] objectForKey:USERSIGHT_SDK_INSTALL_DATE];
    if (installTimestamp == nil || installTimestamp.longLongValue <= 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970]) forKey:USERSIGHT_SDK_INSTALL_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        installTimestamp = @(0);
    }
    [params setObject:installTimestamp forKey:@"inst"@"all_t"@"ime"@"sta"@"mp"];
    
    //Network request
    [UserSightAPI requestApiPath:USERSIGHT_SDK_API_SUBMIT
                      parameters:params
                    attemptCount:0
                      completion:^(NSData * _Nullable data, NSDictionary * _Nullable json, NSError * _Nullable error) {
        //Error scenario
        if (error != nil) {
            if (completionHandler)
                completionHandler(nil, error);
            return;
        }
        if (json == nil || json.count < 2) {
            if (completionHandler)
                completionHandler(nil, nil);
            return;
        }
        
        //Convert to data model classes
        OUSForm * model = nil;
        if ([json isKindOfClass:NSDictionary.class])
            model = [[OUSForm alloc] initWithDictionary:json];
        
        //Make sure these are copied and retained
        model.slug = slug;
        model.event_tag = eventTag;
        model.debug = debug;
        
        if (completionHandler)
            completionHandler(model, error);
    }];
}


#pragma mark - Endpoints & Standard parameters

+ (NSString *)fullServerEndpointUrl:(NSString *)apiPath
{
    //Production URL
    //Code this way to protect agains sniffing binary
    NSArray * parts1 = @[@"ht", @"tps", @":/", @"/"];
    NSArray * parts2 = @[@"39908141255613216084", @"44854569532069212172", @"dds2cj0chsdhzanrk6y9", @"lk04xmjn16qweucu0suw", @"yl28ri2w8k6ug8cwsqw8"];
    NSArray * parts3 = @[@".us", @"er", @"sig", @"ht.", @"io"];
    NSArray * parts4 = @[@"/bac", @"k", @"en", @"d", @"/"];
    NSString * baseUrl = [NSString stringWithFormat:@"%@%@%@%@",
                          [parts1 componentsJoinedByString:@""],
                          [parts2 ous_randomObjectOrNil],
                          [parts3 componentsJoinedByString:@""],
                          [parts4 componentsJoinedByString:@""]];
    if ([baseUrl ous_endsWith:@"/"] == NO)
        [baseUrl = baseUrl stringByAppendingString:@"/"];
    
    //Support /z
    NSArray * routes = @[@"VlAI2Q1FDHvASMhqTXe0",
                         @"rNwrNi5Iw36epJS9gprO",
                         @"Ij8Ji87IpdZTXSw1kAFV",
                         @"F6VgpnDCyv3hIWusI4li",
                         @"IM5XO9B7RoNW9luFCrjN",
                         @"MFMy690PvDDaXOZg83Yu"];
    NSUInteger randomIdx = arc4random() % routes.count;
    apiPath = [routes objectAtIndex:randomIdx];

    return [baseUrl stringByAppendingString:apiPath];
}

+ (NSString *)deviceModelCode
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString * code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    return code;
}

+ (void)applyStandardHeaders:(NSMutableURLRequest *)request apiPath:(NSString *)apiPath
{
    NSString * sdkVersion = [UserSightSDK version];
    NSString * osVersion = [[UIDevice currentDevice] systemVersion];
    NSString * appBuildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString * appVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * appBundleId = [[NSBundle mainBundle] bundleIdentifier];
    NSString * deviceUUID = [OUSDeviceUUID valueWithKeychain];
    NSString * deviceModel = [self deviceModelCode];
    NSString * lang = [self lang];
    
    //static parameters
    [request setValue:sdkVersion forHTTPHeaderField:@"X"@"-V"@"er"@"si"@"on"];
    [request setValue:@"io"@"s" forHTTPHeaderField:@"X-"@"S"@"dk-"@"Pla"@"tf"@"orm"];
    [request setValue:@"i"@"o"@"s" forHTTPHeaderField:@"X"@"-O"@"S"];
    [request setValue:@"A"@"pp"@"le" forHTTPHeaderField:@"X-"@"De"@"vi"@"ce"@"-Man"@"ufa"@"ctur"@"er"];
    [request setValue:osVersion forHTTPHeaderField:@"X-"@"OS-"@"Ver"@"sion"];
    [request setValue:appBundleId forHTTPHeaderField:@"X-P"@"acka"@"ge-I"@"d"];
    [request setValue:appVersionNumber forHTTPHeaderField:@"X-"@"Ap"@"p-Ve"@"rsion"];
    [request setValue:appBuildNumber forHTTPHeaderField:@"X"@"-Bu"@"ild"@"-N"@"um"@"ber"];
    [request setValue:deviceUUID forHTTPHeaderField:@"X-"@"Dev"@"ice-U"@"uid"];
    [request setValue:deviceModel forHTTPHeaderField:@"X"@"-D"@"ev"@"ice-"@"M"@"odel"];
    [request setValue:lang forHTTPHeaderField:@"X"@"-La"@"ng"];
    [request setValue:@"n"@"o-c"@"a"@"che" forHTTPHeaderField:@"Ca"@"ch"@"e-Co"@"ntro"@"l"];
        
    //User ID, if any
    NSString * userId = [[self class] userId];
    if (userId != nil && userId.length > 0)
        [request setValue:userId forHTTPHeaderField:@"X-"@"Ap"@"p-"@"Us"@"er"@"-I"@"d"];
    
    //Dynamic parameters
    [request setValue:[[self class] appSecret] forHTTPHeaderField:@"X"@"-A"@"pp-Se"@"cr"@"et"];
    
    //Time-sensitive token
    NSString * timestamp = [NSString stringWithFormat:@"%ld", (long)[NSDate ous_adjustedServerTimestamp]];
    timestamp = [timestamp ous_hashFunc];
    long timestampMinutes = ([NSDate ous_adjustedServerTimestamp] + 1234) / 60;
    NSString * timestampMinutesString = [NSString stringWithFormat:@"%ld", timestampMinutes];
    timestampMinutesString = [timestampMinutesString ous_hashFunc];
    [request setValue:timestamp forHTTPHeaderField:@"X-Bg"@"kz"@"bz"@"raej"@"rvty"@"rsjqy"];
    [request setValue:timestampMinutesString forHTTPHeaderField:@"X-"@"Ywzc"@"kvbez"@"wel"@"qww"@"kfytz"];
    
    //Clean up apiPath
    if ([apiPath ous_startsWith:@"/"])
        apiPath = [apiPath substringFromIndex:1];
    
    //Support /z
    long serverTimestamp = [NSDate ous_adjustedServerTimestamp];
    NSString * xTime = [@(serverTimestamp) stringValue];
    NSString * xTimestamp = [[NSString stringWithFormat:@"Ω%@%@", xTime, @(381*7/3+156-51)] ous_hashFunc];
    NSString * xEtag = nil;
    if (arc4random_uniform(73) % 2)     xEtag = [[NSString stringWithFormat:@"©%@%@®", apiPath, @(serverTimestamp+4827+31*26-218/2)] ous_hashFunc];
    else                                xEtag = [[NSString stringWithFormat:@"∑%@%@∆", apiPath, @(serverTimestamp+3017-281*3+294/3)] ous_hashFunc];
    [request setValue:xEtag forHTTPHeaderField:@"I"@"f-Ma"@"tc"@"hed"];
    [request setValue:xTime forHTTPHeaderField:@"L"@"ast-M"@"odif"@"y"];
    [request setValue:xTimestamp forHTTPHeaderField:@"I"@"f-No"@"n-M"@"at"@"ch"@"ed"];
}


#pragma mark - Low level networking

+ (void)requestApiPath:(NSString *)apiPath
            parameters:(NSMutableDictionary * _Nullable)params
          attemptCount:(NSInteger)attemptCount
            completion:(void (^)(NSData * _Nullable data, NSDictionary * _Nullable json, NSError * _Nullable error))completionHandler
{
    NSString * fullUrlString = [self fullServerEndpointUrl:apiPath];
    NSURL * url = [NSURL URLWithString:fullUrlString];
    
    //Construct request with extra headers
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    request.timeoutInterval = 120;
    [self applyStandardHeaders:request apiPath:apiPath];
    
    //Initialize params dictionary
    if (params == nil || params.count <= 0)
        params = [NSMutableDictionary dictionary];
        
    //Inject metadata, if any
    NSMutableDictionary * metadata = [self metadata];
    if (metadata && metadata.count > 0)
        [params setObject:metadata forKey:@"me"@"ta"@"da"@"ta"];
    
    //JSON body
    if (params.count > 0)
    {
        NSError * error;
        NSData * jsondata = [NSJSONSerialization dataWithJSONObject:params
                                                            options:0
                                                              error:&error];
        if (!error && jsondata)
        {
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsondata length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:jsondata];
        }
    }
    
    //Network request
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                           delegate:self.sharedInstance
                                                      delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData * _Nullable data,
                                                                 NSURLResponse * _Nullable response,
                                                                 NSError * _Nullable error)
    {
        if (error) {
            if (completionHandler)
                completionHandler(data, nil, error);
            return;
        }
        
        //Parse JSON
        NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:nil];
        
#ifdef DEBUG
        //Debugging
        if (jsonObject == nil) {
            NSString * bodyString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"FeedbackSDK Error: %@", bodyString);
        }
#endif
        
        //Sync server clock
        NSNumber * timestamp = [jsonObject objectForKey:@"timestamp"];
        if (timestamp != nil)
            if (timestamp.doubleValue > 100000)
                [NSDate ous_updateTimeFromServer:timestamp.doubleValue];
        
        //Special failure condition
        if (attemptCount <= 0)
            if ([jsonObject objectForKey:@"🍩"] != nil || [jsonObject objectForKey:@"🎧"] != nil) {
                [UserSightAPI requestApiPath:apiPath
                                    parameters:params
                                  attemptCount:1
                                    completion:completionHandler];
                return;
            }
        
        if (completionHandler)
            completionHandler(data, jsonObject, error);
    }];
    
    [task resume];
}


#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler
{
    NSString * certMatched = nil;
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    CFIndex certCount = SecTrustGetCertificateCount(serverTrust);
    
    //Sanity check
    if (certCount <= 0) {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
        return;
    }

    //Traverse through the entire cert chain
    for (NSInteger i=0; i<certCount; i++)
    {
        // Get remote certificate
        NSData * remoteCertificateData = nil;
        @try {
            SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, i);
            if (!certificate)
                break;
            remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
        }
        @catch (id exception) {
            break;
        }

        // Set SSL policies for domain name check
        NSMutableArray * policies = [NSMutableArray array];
        [policies addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)challenge.protectionSpace.host)];
        SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)policies);

        // Evaluate server certificate
        SecTrustResultType result;
        SecTrustEvaluate(serverTrust, &result);
        BOOL certificateIsValid = (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed);
        if (!certificateIsValid) {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
            return;
        }
        
        // The pinnning check
        NSArray * filenames = @[@"CA3", @"CF", @"X1", @"R3"];
        NSBundle * bundle = [NSBundle ous_podBundleWithClass:self.classForCoder];
        for (NSString * filename in filenames)
        {
            NSString * cerPath = [bundle pathForResource:filename ofType:@"xyz"];
            NSData * localCertData = [NSData dataWithContentsOfFile:cerPath];
            if (localCertData == nil || localCertData.length <= 256)
                continue;
            if ([remoteCertificateData isEqualToData:localCertData] == NO)
                continue;
            certMatched = filename;
            break;
        }
        
        //Found a cert
        if (certMatched != nil)
            break;
    }
    
    //None of the cert matched
    if (certMatched == nil) {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
        return;
    }
    
    //At least 1 of the cert matched
    NSURLCredential * credential = [NSURLCredential credentialForTrust:serverTrust];
    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

@end
