//
//  UILabel+OUSAdditions.m
//

#import "UIView+OUSAdditions.h"
#import "UILabel+OUSAdditions.h"
#import "NSString+OUSAdditions.h"

@implementation UILabel (OUSAdditions)

- (void)ous_sizeToFitKeepHeight
{
    //No content
    if ([self.text ous_trim].length <= 0) {
        self.ous_width = 0;
        return;
    }
    
    CGFloat initialLeft = self.ous_left;
    CGFloat initialRight = self.ous_right;
    CGFloat initialHeight = CGRectGetHeight(self.bounds);
    [self sizeToFit];
    
    self.ous_height = initialHeight;
    
    if (self.textAlignment == NSTextAlignmentLeft)
        self.ous_left = initialLeft;
    else if (self.textAlignment == NSTextAlignmentRight)
        self.ous_right = initialRight;
}

- (void)ous_sizeToFitKeepWidth
{
    //No content
    if ([self.text ous_trim].length <= 0) {
        self.ous_height = 0;
        return;
    }
    
    CGFloat initialLeft = self.ous_left;
    CGFloat initialRight = self.ous_right;
    CGFloat initialWidth = CGRectGetWidth(self.bounds);
    [self sizeToFit];
    
    self.ous_width = initialWidth;
    
    if (self.textAlignment == NSTextAlignmentLeft)
        self.ous_left = initialLeft;
    else if (self.textAlignment == NSTextAlignmentRight)
        self.ous_right = initialRight;
}

- (void)ous_fadeToText:(NSString *)newText
{
    [self ous_fadeToText:newText duration:0.35];
}

- (void)ous_fadeToText:(NSString *)newText duration:(CGFloat)duration
{
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = duration;
    [self.layer addAnimation:animation forKey:@"kCATransitionFade"];
    
    // This will fade due to the above transition already in placed
    self.text = newText;
}

//Shake & Highlight it in red
- (void)ous_shakeXerror
{
    CGFloat previousAlpha = self.alpha;
    UIColor * previousColor = self.textColor;
    self.textColor = [UIColor redColor];
    self.alpha = 1;
    
    [self ous_shakeX];
    
    [UIView animateWithDuration:1 animations:^{
        self.alpha = previousAlpha;
    } completion:nil];
    
    //This must be coded specifically this way for it to have any effect at all
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self ous_addFadeTransitionWithDuration:1.5];
        self.textColor = previousColor;
    });
}

@end
