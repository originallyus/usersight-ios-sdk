#import <UIKit/UIKit.h>

@interface UIButton (OUSAdditions)

@property (nonatomic, strong) UIImage * _Nullable image;
@property (nonatomic, assign) BOOL disabled;

- (void)ous_sizeToFitKeepHeight;
- (void)ous_sizeToFitKeepWidth;
- (void)ous_sizeToFitKeepHeightWithPadding:(CGFloat)padding;
- (void)ous_sizeToFitKeepWidthWithPadding:(CGFloat)padding;

- (void)ous_fadeToText:(NSString * _Nullable)newText;
- (void)ous_fadeToText:(NSString * _Nullable)newText duration:(CGFloat)duration;

@end
