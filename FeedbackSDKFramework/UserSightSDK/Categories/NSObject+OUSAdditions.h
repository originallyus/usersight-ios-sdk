//
//  NSObject+OUSAdditions.h
//

#import <Foundation/Foundation.h>

@interface NSObject (OUSAdditions)

#pragma mark - Callback mechanism

- (void)ous_setCallbackBlock:(void (^_Nullable)(id _Nonnull object))callbackBlock;
- (BOOL)ous_performCallbackBlockWithObject:(id _Nullable)object1;
- (void)ous_performCallbackBlockWithObject:(id _Nullable)object1 delay:(NSTimeInterval)delay;
- (void)ous_clearCallbackBlock;


#pragma mark - Tag an arbirtary object

- (id _Nullable)ous_getTagObject;
- (id _Nullable)ous_getTagObjectForKey:(const void * _Nonnull)key;

- (void)ous_tagObject:(id _Nonnull)object;
- (void)ous_tagObject:(id _Nonnull)object withKey:(const void * _Nonnull)key;

- (void)ous_clearTagObject;
- (void)ous_clearTagObjectForKey:(const void * _Nonnull)key;

@end
