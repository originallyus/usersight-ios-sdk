//
//  UILabel+OUSAdditions.h
//

#import <UIKit/UIKit.h>

@interface UILabel (OUSAdditions)

- (void)ous_sizeToFitKeepHeight;
- (void)ous_sizeToFitKeepWidth;

- (void)ous_fadeToText:(NSString *)newText;
- (void)ous_fadeToText:(NSString *)newText duration:(CGFloat)duration;

- (void)ous_shakeXerror;

@end
