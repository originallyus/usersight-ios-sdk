//
//  NSNumber+OUSAddition.m
//

#import "NSNumber+OUSAdditions.h"

@implementation NSNumber (OUSAddition)

//Avoid crashes when being used wrongly
- (NSUInteger)length
{
    NSLog(@"WARNING: attempt to perform selector 'length' on a NSNumber object");
    return 0;
}

//Avoid crashes when being used wrongly
- (BOOL)isEqualToString:(NSString *)anotherString
{
    return [self.stringValue isEqualToString:anotherString];
}

@end
