#import "NSObject+OUSAdditions.h"
#import <objc/runtime.h>
#import <malloc/malloc.h>
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation NSObject (OUSAdditions)

#pragma mark - Universal callback mechanism

- (void)ous_setCallbackBlock:(void (^)(id object))callbackBlock
{
    //set block as an attribute in runtime
    if (callbackBlock)
        objc_setAssociatedObject(self, "ei17Kq4Om1", [callbackBlock copy], OBJC_ASSOCIATION_COPY);
}

//Return YES if there is a block object
- (BOOL)ous_performCallbackBlockWithObject:(id)object1
{
    //get back the block object attribute we set earlier
    void (^block)(id obj) = objc_getAssociatedObject(self, "ei17Kq4Om1");
    if (block) {
        block(object1);
        return YES;
    }
    
    return NO;
}

- (void)ous_performCallbackBlockWithObject:(id)object1 delay:(NSTimeInterval)delay
{
    [self performSelector:@selector(ous_performCallbackBlockWithObject:) withObject:object1 afterDelay:delay];
}

- (void)ous_clearCallbackBlock
{
    objc_setAssociatedObject(self, "ei17Kq4Om1", nil, OBJC_ASSOCIATION_COPY);
}


#pragma mark - Tag an arbirtary object

- (id _Nullable)ous_getTagObject
{
    return objc_getAssociatedObject(self, "f1kA70gZ9");
}

- (id _Nullable)ous_getTagObjectForKey:(const void * _Nonnull)key
{
    return objc_getAssociatedObject(self, key);
}

- (void)ous_tagObject:(id)object
{
    objc_setAssociatedObject(self, "f1kA70gZ9", object, OBJC_ASSOCIATION_RETAIN);
}

- (void)ous_tagObject:(id)object withKey:(const void * _Nonnull)key
{
    objc_setAssociatedObject(self, key, object, OBJC_ASSOCIATION_RETAIN);
}

- (void)ous_clearTagObject
{
    objc_setAssociatedObject(self, "f1kA70gZ9", nil, OBJC_ASSOCIATION_RETAIN);
}

- (void)ous_clearTagObjectForKey:(const void * _Nonnull)key
{
    objc_setAssociatedObject(self, key, nil, OBJC_ASSOCIATION_RETAIN);
}

@end
