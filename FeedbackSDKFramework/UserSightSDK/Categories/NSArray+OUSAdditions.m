//
//  NSArray+OUSAdditions.m
//

#import "NSArray+OUSAdditions.h"

@implementation NSArray (OUSAdditions)

- (NSArray *)ous_reverse
{
    NSArray *reversedArray = [[self reverseObjectEnumerator] allObjects];
    return reversedArray;
}

- (NSMutableArray *)ous_arrayWithFirstNobject:(NSInteger)count
{
    NSMutableArray * array = [NSMutableArray array];
    
    for (id obj in self) {
        if ([array count] >= count)
            break;
        [array addObject:obj];
    }
    
    return array;
}


#pragma mark -

- (id)ous_safeObjectAtIndex:(NSUInteger)index
{
    if (index >= [self count])
        return nil;
    
    return [self objectAtIndex:index];
}

- (id)ous_firstObjectOrNil
{
    if ([self count] <= 0)
        return nil;
    
    return [self objectAtIndex:0];
}

- (id)ous_lastObjectOrNil
{
    if ([self count] <= 0)
        return nil;
    
    return [self objectAtIndex:self.count-1];
}

- (id)ous_randomObjectOrNil
{
    if ([self count] <= 0)
        return nil;
    
    if ([self count] == 1)
        return [self ous_firstObjectOrNil];
    
    int n = arc4random_uniform((u_int32_t)[self count]);
    return [self objectAtIndex:n];
}

- (NSArray *)ous_firstNObjects:(NSInteger)n
{
    if (n > self.count)
        n = self.count;
    return [self subarrayWithRange:NSMakeRange(0, n)];
}

- (NSUInteger)ous_length
{
    return [self count];
}

- (id)ous_firstObjectWithValue:(id)value forKeyPath:(NSString*)key
{
    for (id object in self)
        if ([[object valueForKey:key] isEqual:value])
            return object;
    
    return nil;
}

- (NSArray *)ous_filteredArrayWithValue:(id)value forKeyPath:(NSString*)key
{
    NSMutableArray * objects = [NSMutableArray array];
    
    for (id object in self)
        if ([[object valueForKey:key] isEqual:value])
            [objects addObject:object];
    
    return [NSArray arrayWithArray:objects];
}

@end
