//
//  NSNumber+OUSAddition.h
//

#import <Foundation/Foundation.h>

@interface NSNumber (OUSAddition)

//Name of these functions are mimicking NSString on purpose

//Avoid crashes when being used wrongly
- (NSUInteger)length;

//Avoid crashes when being used wrongly
- (BOOL)isEqualToString:(NSString *)anotherString;

@end
