#import <CoreText/CoreText.h>
#import "UIFont+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"
#import "OUSFont.h"                 //For NSBundle to point to the correct one

@implementation UIFont (OUSAdditions)

+ (BOOL)ous_loadFontName:(NSString *)postScriptName filename:(NSString *)fontFilename url:(NSString *)fontUrl
{
    //Check already loaded
    UIFont * alreadyLoadedFont = [UIFont fontWithName:postScriptName size:10];
    if (alreadyLoadedFont)
        return YES;
    
    //Split filename & extension
    NSString * extension = [fontFilename pathExtension];
    NSString * filenameOnly = [fontFilename stringByDeletingPathExtension];
    
    //Sanity checks
    if (!extension || extension.length <= 0) {
        NSLog(@"[UserSightSDK] Invalid file extension for font file: %@", fontFilename);
        return NO;
    }
    if (!filenameOnly || filenameOnly.length <= 0) {
        NSLog(@"[UserSightSDK] Invalid filenameOnly for font file: %@", fontFilename);
        return NO;
    }
    
    //Load from the main app bundle first
    NSString * fontPath = [[NSBundle mainBundle] pathForResource:filenameOnly ofType:extension];

    //Cocoapod framework bundle
    if (fontPath == nil) {
        NSBundle * bundle = [NSBundle ous_podBundleWithClass:OUSFont.class];
        fontPath = [bundle pathForResource:filenameOnly ofType:extension];
    }
    
    //Cordova
    if (fontPath == nil)
        fontPath = [[NSBundle mainBundle] pathForResource:filenameOnly ofType:extension inDirectory:@"www"];
    if (fontPath == nil)
        fontPath = [[NSBundle mainBundle] pathForResource:filenameOnly ofType:extension inDirectory:@"www/assets"];
    if (fontPath == nil)
        fontPath = [[NSBundle mainBundle] pathForResource:filenameOnly ofType:extension inDirectory:@"www/assets/fonts"];
    
    //Cannot find locally, try to load from network
    if (fontPath == nil) {
        [self ous_downloadFontUrl:fontUrl fontName:postScriptName];
        return NO;
    }
    
    //Load font data locally
    NSData * fontData = [NSData dataWithContentsOfFile:fontPath];
    return [self ous_loadFontData:fontData fontName:postScriptName];
}

+ (void)ous_downloadFontUrl:(NSString *)fontUrl fontName:(NSString *)postScriptName
{
    //Sanity checks
    NSURL * url = [NSURL URLWithString:fontUrl];
    if (!url)
        return;
    
    if (postScriptName)         NSLog(@"[UserSightSDK] Loading '%@' font from URL: %@", postScriptName, fontUrl);
    else                        NSLog(@"[UserSightSDK] Loading font from URL: %@", fontUrl);
    
    NSURLSessionDataTask * downloadTask = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                                      completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        if (error != nil) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        if (data == nil) {
            NSLog(@"[UserSightSDK] Downloaded font data is nil from URL: %@", fontUrl);
            return;
        }
        
        BOOL success = [self ous_loadFontData:data fontName:postScriptName];
        if (success)
            NSLog(@"[UserSightSDK] Loaded '%@' font from network", postScriptName);
    }];    
    [downloadTask resume];
}

+ (BOOL)ous_loadFontData:(NSData *)fontData fontName:(NSString *)postScriptName
{
    //Sanity checks
    if (!fontData) {
        NSLog(@"[UserSightSDK] Invalid NSData for font: %@", postScriptName);
        return NO;
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
    if (!provider)
        return NO;
    
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    if (font)
    {
        CFErrorRef error = NULL;
        if (CTFontManagerRegisterGraphicsFont(font, &error) == NO)
        {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            NSLog(@"[UserSightSDK] Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        CFRelease(font);
    }
    CFRelease(provider);
    
    return YES;
}

@end
