#import <UIKit/UIKit.h>

@interface NSBundle (OUSAdditions)

+ (NSBundle * _Nullable)ous_podBundleWithClass:(Class _Nonnull)classObject;

@end
