#import "UIView+OUSAdditions.h"
#import "UIColor+OUSAdditions.h"
#import "UIButton+OUSAdditions.h"

@implementation UIButton (OUSAdditions)

- (BOOL)disabled
{
    return !self.enabled;
}
- (void)setDisabled:(BOOL)disabled
{
    self.enabled = !disabled;
}

- (UIImage *)image
{
    return self.imageView.image;
}
- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

- (void)ous_sizeToFitKeepHeight
{
    [self ous_sizeToFitKeepHeightWithPadding:0];
}

- (void)ous_sizeToFitKeepHeightWithPadding:(CGFloat)padding
{
    CGFloat initialLeft = self.ous_left;
    CGFloat initialRight = self.ous_right;
    CGFloat initialHeight = CGRectGetHeight(self.bounds);
    [self sizeToFit];
    
    self.ous_width += padding * 2;
    self.ous_height = initialHeight;
    
    if (self.titleLabel.textAlignment == NSTextAlignmentLeft)
        self.ous_left = initialLeft;
    else if (self.titleLabel.textAlignment == NSTextAlignmentRight)
        self.ous_right = initialRight;
}

- (void)ous_sizeToFitKeepWidth
{
    [self ous_sizeToFitKeepWidthWithPadding:0];
}

- (void)ous_sizeToFitKeepWidthWithPadding:(CGFloat)padding
{
    CGFloat initialLeft = self.ous_left;
    CGFloat initialRight = self.ous_right;
    CGFloat initialWidth = CGRectGetWidth(self.bounds);
    [self sizeToFit];
    
    self.ous_height += padding * 2;
    self.ous_width = initialWidth;
    
    if (self.titleLabel.textAlignment == NSTextAlignmentLeft)
        self.ous_left = initialLeft;
    else if (self.titleLabel.textAlignment == NSTextAlignmentRight)
        self.ous_right = initialRight;
}

- (void)ous_fadeToText:(NSString *)newText
{
    [self ous_fadeToText:newText duration:0.35];
}

- (void)ous_fadeToText:(NSString *)newText duration:(CGFloat)duration
{
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = duration;
    [self.layer addAnimation:animation forKey:@"kCATransitionFade"];
    
    [self setTitle:newText forState:UIControlStateNormal];
}

@end
