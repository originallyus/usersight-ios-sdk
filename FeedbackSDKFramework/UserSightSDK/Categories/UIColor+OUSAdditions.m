#import "UIColor+OUSAdditions.h"

@implementation UIColor (OUSAdditions)

+ (UIColor *)ous_colorWithHexValue:(NSUInteger)hexValue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:(((hexValue & 0xFF0000) >> 16)) / 255.0
                           green:(((hexValue & 0xFF00) >> 8)) / 255.0
                            blue:((hexValue & 0xFF)) / 255.0
                           alpha:alpha];
}

+ (UIColor *)ous_colorWithHexValue:(NSUInteger)hexValue
{
    return [self ous_colorWithHexValue:hexValue alpha:1];
}

+ (UIColor *)ous_colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha
{
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if ([hexString length] != 3 && [hexString length] != 6 && [hexString length] != 8)
        return nil;
    
    if ([hexString length] == 3) {
        NSString *r = [hexString substringWithRange:NSMakeRange(0, 1)];
        NSString *g = [hexString substringWithRange:NSMakeRange(1, 1)];
        NSString *b = [hexString substringWithRange:NSMakeRange(2, 1)];
        hexString = [NSString stringWithFormat:@"%@%@%@%@%@%@", r, r, g, g, b, b];
    }
    
    if ([hexString length] == 8) {
        NSString *a = [hexString substringWithRange:NSMakeRange(0, 2)];
        unsigned int alphaHexValue;
        if ([[NSScanner scannerWithString:a] scanHexInt:&alphaHexValue])
            alpha = alphaHexValue / 255.0;
        
        hexString = [hexString substringFromIndex:2];
    }
    
    UIColor *color = nil;
    unsigned int hexValue;
    if ([[NSScanner scannerWithString:hexString] scanHexInt:&hexValue]) {
        color = [self ous_colorWithHexValue:hexValue alpha:alpha];
    }
    return [color colorWithAlphaComponent:alpha];
}

+ (UIColor *)ous_colorWithHexString:(NSString *)hexString
{
    return [self ous_colorWithHexString:hexString alpha:1];
}


#pragma mark -

- (UIColor *)ous_lighterColor
{
    return [self ous_colorWithBrightnessFactor:1.30];
}

- (UIColor *)ous_darkerColor
{
    return [self ous_colorWithBrightnessFactor:0.70];
}

//Brighter color with factor > 1.0, Darker color with factor < 1
- (UIColor *)ous_colorWithBrightnessFactor:(CGFloat)brightnessFactor
{
    CGFloat h, s, b, a;
    
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
    {
        CGFloat newBrightness = MAX( MIN(b * brightnessFactor, 1.0), 0 );
        
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:newBrightness
                               alpha:a];
    }
    return self;
}

@end
