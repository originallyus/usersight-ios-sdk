#import <AVFoundation/AVFoundation.h>
#import "UIView+OUSAdditions.h"

@implementation UIView (OUSAdditions)

- (BOOL)ous_visible {
    return !self.hidden;
}

- (void)setOus_visible:(BOOL)visible {
    self.hidden = !visible;
}

- (CGFloat)ous_left {
  return self.frame.origin.x;
}

- (void)setOus_left:(CGFloat)x {
  CGRect frame = self.frame;
  frame.origin.x = x;
  self.frame = frame;
}

- (CGFloat)ous_top {
  return self.frame.origin.y;
}

- (void)setOus_top:(CGFloat)y {
  CGRect frame = self.frame;
  frame.origin.y = y;
  self.frame = frame;
}

- (CGFloat)ous_right {
  return self.frame.origin.x + self.frame.size.width;
}

- (void)setOus_right:(CGFloat)right {
  CGRect frame = self.frame;
  frame.origin.x = right - frame.size.width;
  self.frame = frame;
}

- (CGFloat)ous_bottom {
  return self.frame.origin.y + self.frame.size.height;
}

- (void)setOus_bottom:(CGFloat)bottom {
  CGRect frame = self.frame;
  frame.origin.y = bottom - frame.size.height;
  self.frame = frame;
}

- (CGFloat)ous_centerX {
  return self.center.x;
}

- (void)setOus_centerX:(CGFloat)centerX {
  self.center = CGPointMake(centerX, self.center.y);
}

- (CGFloat)ous_centerY {
  return self.center.y;
}

- (void)setOus_centerY:(CGFloat)centerY {
  self.center = CGPointMake(self.center.x, centerY);
}

- (CGFloat)ous_width {
  return self.bounds.size.width;
}

- (void)setOus_width:(CGFloat)width {
  CGRect frame = self.frame;
  frame.size.width = width;
  self.frame = frame;
}

- (CGFloat)ous_height {
  return self.bounds.size.height;
}

- (void)setOus_height:(CGFloat)height {
  CGRect frame = self.frame;
  frame.size.height = height;
  self.frame = frame;
}

- (CGPoint)ous_contentCenter
{
    return CGPointMake(floorf(self.ous_width/2.0f), floorf(self.ous_height/2.0f));
}


#pragma mark -

- (void)ous_removeAllSubviews
{
    //NSLog(@"removeAllSubviews in OUSAdditions");
    
    while ([self.subviews count] > 0)
        [[self.subviews firstObject] removeFromSuperview];
}

- (void)ous_bringToFront
{
    [self.superview bringSubviewToFront:self];
}


#pragma mark -

- (UIView *)ous_firstResponder
{
    if ([self isFirstResponder])
        return self;

    for (UIView * subview in self.subviews) {
        UIView * firstResponder = [subview ous_firstResponder];
        if (firstResponder)
            return firstResponder;
    }

    return nil;
}

- (BOOL)ous_hasFirstResponder
{
    if ([self isFirstResponder])
        return YES;

    for (UIView * subview in self.subviews)
        if ([subview ous_hasFirstResponder])
            return YES;
    
    return NO;
}


#pragma mark -

- (void)ous_roundCorner
{
    CGFloat size = MIN(self.frame.size.width,
                      self.frame.size.height);
    
    self.layer.cornerRadius = size / 2;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
}

- (void)ous_roundCornerWithRadius:(CGFloat)radius
{
    CGFloat size = MIN(self.frame.size.width,
                      self.frame.size.height);
    radius = MIN(radius, size/2);
    
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
}

- (void)ous_roundCorners:(UIRectCorner)corners withRadius:(CGFloat)radius
{
    UIBezierPath * maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                    byRoundingCorners:corners
                                                          cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer * maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
}


#pragma mark - Animation

- (void)ous_addFadeTransitionWithDuration:(CGFloat)duration
{
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = duration;
    [self.layer addAnimation:animation forKey:@"kCATransitionFade"];
}

- (void)ous_fadeInWithDuration:(CGFloat)duration
{
    if (duration <= 0) {
        self.alpha = 1;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        weakSelf.alpha = 1;
    } completion:^(BOOL finished) {
        weakSelf.alpha = 1;
    }];
}

- (void)ous_fadeInWithDuration:(CGFloat)duration completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        self.alpha = 1;
        if (completion)
            completion(YES);
        return;
    }
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        self.alpha = 1;
        if (completion)
            completion(finished);
    }];
}

- (void)ous_fadeInWithDuration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.alpha = 0;
        });
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.alpha = 1;
    } completion:^(BOOL finished) {
        weakSelf.alpha = 1;
    }];
}

- (void)ous_fadeInWithDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.alpha = 0;
            if (completion)
                completion(YES);
        });
        return;
    }
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        self.alpha = 1;
        if (completion)
            completion(finished);
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration
{
    if (duration <= 0) {
        self.alpha = 0;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        weakSelf.alpha = 0;
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        self.alpha = 0;
        if (completion)
            completion(YES);
        return;
    }
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.alpha = 0;
        if (completion)
            completion(finished);
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration andRemoveFromSuperView:(BOOL)toBeRemoved
{
    if (duration <= 0) {
        self.alpha = 0;
        if (toBeRemoved)
            [self removeFromSuperview];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        if (toBeRemoved)
            [weakSelf removeFromSuperview];
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration andRemoveFromSuperView:(BOOL)toBeRemoved completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        self.alpha = 0;
        if (toBeRemoved)
            [self removeFromSuperview];
        if (completion)
            completion(YES);
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (toBeRemoved)
            [weakSelf removeFromSuperview];
        if (completion)
            completion(finished);
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.alpha = 0;
        });
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        weakSelf.alpha = 0;
    }];
}

- (void)ous_fadeOutWithDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.alpha = 0;
            if (completion)
                completion(YES);
        });
        return;
    }
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.alpha = 0;
        if (completion)
            completion(finished);
    }];
}

- (void)ous_fadeToAlpha:(CGFloat)alpha
{
    [self ous_fadeToAlpha:alpha withDuration:0.35];
}

- (void)ous_fadeToAlpha:(CGFloat)alpha completion:(void (^ __nullable)(BOOL finished))completion
{
    [self ous_fadeToAlpha:alpha withDuration:0.35 delay:0 completion:completion];
}

- (void)ous_fadeToAlpha:(CGFloat)alpha withDuration:(CGFloat)duration
{
    if (duration <= 0) {
        self.alpha = alpha;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        weakSelf.alpha = alpha;
    } completion:^(BOOL finished) {
        weakSelf.alpha = alpha;
    }];
}

- (void)ous_fadeToAlpha:(CGFloat)alpha withDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion
{
    if (duration <= 0) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.alpha = alpha;
            if (completion)
                completion(YES);
        });
        return;
    }
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = alpha;
    } completion:^(BOOL finished) {
        self.alpha = alpha;
        if (completion)
            completion(finished);
    }];
}

- (void)ous_animateTopToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_top = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_top = val;
    } completion:nil];
}

- (void)ous_animateBottomToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_bottom = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_bottom = val;
    } completion:nil];
}

- (void)ous_animateLeftToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_left = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_left = val;
    } completion:nil];
}

- (void)ous_animateRightToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_right = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_right = val;
    } completion:nil];
}

- (void)ous_animateWidthToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_width = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_width = val;
    } completion:nil];
}

- (void)ous_animateHeightToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_height = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_height = val;
    } completion:nil];
}

- (void)ous_animateCenterXToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_centerX = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_centerX = val;
    } completion:nil];
}

- (void)ous_animateCenterYToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.ous_centerY = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.ous_centerY = val;
    } completion:nil];
}

- (void)ous_animateFrameToValue:(CGRect)val duration:(CGFloat)duration delay:(CGFloat)delay
{
    if (duration <= 0) {
        self.frame = val;
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        weakSelf.frame = val;
    } completion:nil];
}


#pragma mark -

- (UITapGestureRecognizer *)ous_addTapGestureWithTarget:(id)target action:(nullable SEL)action
{
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:recognizer];
    return recognizer;
}


#pragma mark - Shake animation

- (void)ous_shakeX
{
    [self ous_shakeXWithOffset:40.0 breakFactor:0.85 duration:1.5 maxShakes:30];
}

- (void)ous_shakeXWithOffset:(CGFloat)aOffset
             breakFactor:(CGFloat)aBreakFactor
                duration:(CGFloat)aDuration maxShakes:(NSInteger)maxShakes
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [animation setDuration:aDuration];
    
    
    NSMutableArray *keys = [NSMutableArray arrayWithCapacity:20];
    NSInteger infinitySec = maxShakes;
    while(aOffset > 0.01) {
        [keys addObject:[NSValue valueWithCGPoint:CGPointMake(self.center.x - aOffset, self.center.y)]];
        aOffset *= aBreakFactor;
        [keys addObject:[NSValue valueWithCGPoint:CGPointMake(self.center.x + aOffset, self.center.y)]];
        aOffset *= aBreakFactor;
        infinitySec--;
        if(infinitySec <= 0) {
            break;
        }
    }
    
    animation.values = keys;
    
    
    [self.layer addAnimation:animation forKey:@"position"];
}

@end
