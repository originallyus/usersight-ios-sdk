//
//  NSString+OUSAdditions.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (OUSAdditions)

- (BOOL)ous_isNumber;
- (BOOL)ous_isHttpUrl;


#pragma mark -

- (BOOL)ous_contains:(NSString*)needle;
- (BOOL)ous_startsWith:(NSString*)needle;
- (BOOL)ous_endsWith:(NSString*)needle;
- (BOOL)ous_isEqualToStringCaseNonsensitive:(NSString *)anotherString;
- (BOOL)ous_isEqualToStringIgnoreCase:(NSString *)anotherString;


#pragma mark -

- (NSString *)ous_firstNCharacter:(NSUInteger)n;
- (NSString *)ous_upperCaseFirstCharacter;
- (NSString *)ous_lowerCaseFirstCharacter;


#pragma mark -

- (NSString *)ous_trim;
- (NSString *)ous_hashFunc;

@end
