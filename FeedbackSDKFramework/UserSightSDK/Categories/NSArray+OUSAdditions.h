//
//  NSArray+OUSAdditions.h
//

#import <Foundation/Foundation.h>

@interface NSArray (OUSAdditions)

- (NSArray *)ous_reverse;

- (NSMutableArray *)ous_arrayWithFirstNobject:(NSInteger)count;


#pragma mark -

- (id)ous_safeObjectAtIndex:(NSUInteger)index;
- (id)ous_firstObjectOrNil;
- (id)ous_lastObjectOrNil;
- (id)ous_randomObjectOrNil;
- (NSArray *)ous_firstNObjects:(NSInteger)n;

- (NSUInteger)ous_length;

/*
 * KVC related addition : find and return the first object in the array whose value for keypath *keypath* is equal to *value*.
 * will return nil if no such object is found.
 */
- (id)ous_firstObjectWithValue:(id)value forKeyPath:(NSString*)keypath;

/*
 * KVC related addition : find and return the objects in the array whose value for keypath *keypath* is equal to *value*.
 * will return an empty array if no such object is found.
 */
- (NSArray *)ous_filteredArrayWithValue:(id)value forKeyPath:(NSString*)keypath;

@end
