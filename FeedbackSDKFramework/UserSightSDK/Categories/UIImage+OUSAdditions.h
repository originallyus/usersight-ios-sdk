//
//  UIImage+OUSAdditions.h
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface UIImage (OUSAdditions)

- (UIImage *)ous_changeColor:(UIColor *)color;

@end
