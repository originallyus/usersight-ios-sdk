#import <UIKit/UIKit.h>

@interface UIFont (OUSAdditions)

+ (BOOL)ous_loadFontName:(NSString *)postScriptName filename:(NSString *)fontFilename url:(NSString *)fontUrl;

@end
