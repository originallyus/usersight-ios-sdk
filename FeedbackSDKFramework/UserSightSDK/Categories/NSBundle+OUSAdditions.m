#import "NSBundle+OUSAdditions.h"

@implementation NSBundle (OUSAdditions)

+ (NSBundle * _Nullable)ous_podBundleWithClass:(Class _Nonnull)classObject
{
    //Retrieve the current bundle used by our Pod (not the main bundle)
    NSBundle * bundle = [NSBundle bundleForClass:classObject];
    
    //Fallback: search for Pod bundle inside main bundle
    NSURL * bundleUrl = [bundle URLForResource:@"UserSightSDK" withExtension:@"bundle"];
    if (bundleUrl) {
        NSBundle * tmpBundle = [NSBundle bundleWithURL:bundleUrl];
        if (tmpBundle)
            bundle = tmpBundle;
    }
    
    return bundle;
}

@end
