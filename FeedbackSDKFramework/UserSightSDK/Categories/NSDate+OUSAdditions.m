#import "NSDate+OUSAdditions.h"

@implementation NSDate (OUSAdditions)

#define TIME_INTERVAL_OFFSET                @"vNHhwdd9uvTFUqUEnjbJ"

+ (void)ous_updateTimeFromServer:(NSTimeInterval)timestamp
{
    //Sanity check
    if (timestamp <= 0)
        return;
    
    NSTimeInterval nowTimestamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval delta = timestamp - nowTimestamp;
    
    [[NSUserDefaults standardUserDefaults] setObject:@(delta) forKey:TIME_INTERVAL_OFFSET];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSTimeInterval)ous_adjustedServerTimestamp
{
    NSTimeInterval nowTimestamp = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval delta = [[[NSUserDefaults standardUserDefaults] objectForKey:TIME_INTERVAL_OFFSET] floatValue];
    return nowTimestamp + delta;
}

@end
