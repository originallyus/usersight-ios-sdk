//
//  NSString+OUSAdditions.m
//

#import <time.h>
#import "NSString+OUSAdditions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Additions)

- (BOOL)ous_isNumber
{
  NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
  NSRange r = [self rangeOfCharacterFromSet: nonNumbers];
  return r.location == NSNotFound;
}

- (BOOL)ous_isHttpUrl
{
    NSString *tempString = [self lowercaseString];
    
    //Must begin with 'http' (or https)
    if ([tempString hasPrefix:@"http"] == NO)
        return NO;
    
    //Check has :// followed after http or https
    tempString = [tempString stringByReplacingOccurrencesOfString:@"https" withString:@""];
    tempString = [tempString stringByReplacingOccurrencesOfString:@"http" withString:@""];
    if ([tempString hasPrefix:@"://"] == NO)
        return NO;
    
    return YES;
}


#pragma mark -

- (BOOL)ous_contains:(NSString*)needle {
    NSRange range = [self rangeOfString:needle options: NSCaseInsensitiveSearch];
    return (range.length == needle.length && range.location != NSNotFound);
}

- (BOOL)ous_startsWith:(NSString*)needle {
    return [self hasPrefix:needle];
}

- (BOOL)ous_endsWith:(NSString*)needle {
    return [self hasSuffix:needle];
}

- (BOOL)ous_isEqualToStringCaseNonsensitive:(NSString *)anotherString
{
    return [self.lowercaseString isEqualToString:anotherString.lowercaseString];
}

- (BOOL)ous_isEqualToStringIgnoreCase:(NSString *)anotherString
{
    return [[self uppercaseString] isEqualToString:[anotherString uppercaseString]];
}


#pragma mark -

- (NSString *)ous_firstNCharacter:(NSUInteger)n
{
    if ([self length] < n)
        return self;
    
    return [self substringToIndex:n];
}

- (NSString *)ous_lowerCaseFirstCharacter
{
    if ([self length] <= 1)
        return [self lowercaseString];
    
    NSString *firstCapChar = [[self substringToIndex:1] lowercaseString];
    NSString *cappedString = [self stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    return cappedString;
}

- (NSString *)ous_upperCaseFirstCharacter
{
    if ([self length] <= 1)
        return [self uppercaseString];
    
    NSString *firstCapChar = [[self substringToIndex:1] uppercaseString];
    NSString *cappedString = [self stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    return cappedString;
}


#pragma mark -

- (NSString *)ous_trim
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)ous_hashFunc
{
    const char * str = [self UTF8String];
    if (!str)
        return nil;
    
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG)strlen(str), result);

    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for (int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
        [ret appendFormat:@"%02x",result[i]];

    return ret;
}

@end
