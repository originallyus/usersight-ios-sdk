
#import "OUSDeviceUUID.h"
#import "OUSKeychain.h"

@implementation OUSDeviceUUID

#define KEYCHAIN_STRING_PRODUCTION          @"ZYH7yCfiuCP96M0g"

//
// Hashed of value, with additional keychain storage
//
+ (NSString *)valueWithKeychain
{
    //Return value from keychain if it is there
    NSString * existingValue = [OUSKeychain passwordForService:KEYCHAIN_STRING_PRODUCTION account:KEYCHAIN_STRING_PRODUCTION];
    if (existingValue != nil && [existingValue length] >= 10)
        return existingValue;
    
    //Generate new one
    NSString * newValue = [[NSUUID UUID] UUIDString];
    [OUSKeychain setPassword:newValue forService:KEYCHAIN_STRING_PRODUCTION account:KEYCHAIN_STRING_PRODUCTION];
    
    NSLog(@"OpenUDID value is not available in keychain. Generated new UUID: %@", newValue);
    return newValue;
}

@end
