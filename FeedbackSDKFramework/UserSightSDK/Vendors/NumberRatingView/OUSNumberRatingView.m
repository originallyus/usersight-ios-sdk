#import "OUSNumberRatingView.h"
#import "NSObject+OUSAdditions.h"
#import "UIView+OUSAdditions.h"

#define AUTORESIZING_MASK_LEFT_RIGHT                (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)
#define AUTORESIZING_MASK_TOP_BOTTOM                (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin)
#define AUTORESIZING_MASK_WIDTH_HEIGHT              (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)
#define AUTORESIZING_MASK_ALL_SIDES                 (AUTORESIZING_MASK_TOP_BOTTOM | AUTORESIZING_MASK_LEFT_RIGHT)
#define AUTORESIZING_MASK_ALL                       (AUTORESIZING_MASK_WIDTH_HEIGHT | AUTORESIZING_MASK_ALL_SIDES)

@interface OUSNumberRatingView ()
@property (nonatomic, assign) NSUInteger selectedValue;
@property (nonatomic, strong) NSMutableArray * buttonsArray;
@end

@implementation OUSNumberRatingView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self == nil)
        return nil;
    
    [self setup];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
    
    [self setup];
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setup];
}

- (void)setup
{
    self.autoresizesSubviews = YES;
    
    self.buttonsArray = [NSMutableArray array];
}

- (void)dealloc
{
    for (UIButton * btn in self.buttonsArray)
        [btn removeFromSuperview];
    
    [self.buttonsArray removeAllObjects];
    self.buttonsArray = nil;
    
    self.theme = nil;
}


#pragma mark -

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    // UIView was removed from superview
    if (newSuperview == nil) {
        
        return;
    }
    
    [self setupWithData];

    [super willMoveToSuperview:newSuperview];
}

- (void)setupWithData
{
    [self constructButtons];
    
    self.layer.borderWidth = 1;
    if (self.theme.rating_number_border_color)
        self.layer.borderColor = self.theme.rating_number_border_color.CGColor;
    
    [self ous_roundCornerWithRadius:8];
}


#pragma mark - Construct buttons

- (void)constructButtons
{
    NSInteger count = self.maximumValue - self.minimumValue + 1;
    CGFloat button_width = (self.bounds.size.width / count);
    
    NSInteger index = 0;
    for (NSInteger i=self.minimumValue; i<=self.maximumValue; i++)
    {
        UIButton * btn = [self constructSingleButton:i index:index width:button_width];
        if (btn == nil)
            continue;
        
        [self.buttonsArray addObject:btn];
        index++;
    }
}

- (UIButton *)constructSingleButton:(NSInteger)value index:(NSInteger)index width:(CGFloat)button_width
{
    NSString * title = [NSString stringWithFormat:@"%@", @(value)];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.ous_left = (button_width * index);
    btn.ous_width = button_width+1;
    btn.ous_height = self.ous_height;
    btn.tag = value;
    btn.autoresizingMask = AUTORESIZING_MASK_ALL;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:self.theme.rating_number_text_color forState:UIControlStateNormal];
    [btn setTitleColor:self.theme.rating_number_selected_text_color forState:UIControlStateSelected];
    btn.titleLabel.font = [UIFont fontWithName:self.theme.rating_number_font size:self.theme.rating_number_fontsize.floatValue];
    
    //Special rounded corners
    /*
    if (index <= 0)
        [btn ous_roundCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft withRadius:8];
    if (value == self.maximumValue)
        [btn ous_roundCorners:UIRectCornerTopRight|UIRectCornerBottomRight withRadius:8];
     */
    
    //Border
    btn.layer.borderWidth = 1;
    if (self.theme.rating_number_border_color)
        btn.layer.borderColor = self.theme.rating_number_border_color.CGColor;
        
    [btn addTarget:self action:@selector(onBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    return btn;
}


#pragma mark - Actions

- (IBAction)onBtn:(UIButton *)sender
{
    [sender ous_bringToFront];
    
    //Update styles of individual buttons
    for (UIButton * btn in self.buttonsArray)
        if ([btn isKindOfClass:UIButton.class])
        {
            [btn ous_addFadeTransitionWithDuration:0.2];
            BOOL selected = [btn isEqual:sender];
            btn.selected = selected;
            
            UIColor * bgColor = selected ? self.theme.rating_number_selected_bg_color : self.theme.rating_number_bg_color;
            btn.backgroundColor = bgColor;
            
            UIColor * borderColor = selected ? self.theme.rating_number_selected_border_color : self.theme.rating_number_border_color;
            if (borderColor)
                btn.layer.borderColor = borderColor.CGColor;
        }
    
    //Callback to superview
    [self ous_performCallbackBlockWithObject:self];
}


#pragma mark -

- (NSInteger)value
{
    for (UIButton * btn in self.buttonsArray)
        if ([btn isKindOfClass:UIButton.class])
            if (btn.isSelected)
                return btn.tag;
    
    return 0;
}

@end
