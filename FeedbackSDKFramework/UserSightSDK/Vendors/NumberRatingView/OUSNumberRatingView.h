//
//  OUSFeedbackBaseVC.h
//  UserSightSDK
//
//  Created by Torin Nguyen on 23/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OUSTheme.h"

typedef BOOL(^OUSNumberRatingViewShouldBeginGestureRecognizerBlock)(UIGestureRecognizer *gestureRecognizer);

IB_DESIGNABLE
@interface OUSNumberRatingView : UIView
@property (nonatomic, assign) NSInteger maximumValue;
@property (nonatomic, assign) NSInteger minimumValue;
@property (nonatomic, assign) NSInteger value;
@property (nonatomic, strong) OUSTheme * theme;

// Optional: if `nil` method will return `NO`.
@property (nonatomic, copy) OUSNumberRatingViewShouldBeginGestureRecognizerBlock shouldBeginGestureRecognizerBlock;

@end

