//
//  OUSPassThroughView.m
//
//  Created by Torin Nguyen on 12/12/14.
//  Copyright (c) 2014 Originally US. All rights reserved.
//

#import "OUSPassThroughView.h"

@interface OUSPassThroughView()
@property (nonatomic, assign) BOOL passThroughEnabled;
@end

@implementation OUSPassThroughView

- (void)allowPassThrough:(BOOL)allowed
{
    self.passThroughEnabled = allowed;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView * hitTestView = [super hitTest:point withEvent:event];
    if (hitTestView == self)
        return self.passThroughEnabled ? nil : self;

    return hitTestView;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    //Disabled pass through
    if (!self.passThroughEnabled)
        return [super pointInside:point withEvent:event];
    
    for (UIView * subview in self.subviews)
        if (subview.tag >= OUS_PASS_THROUGH_VIEW_TAGS || [subview isKindOfClass:[UIButton class]])
            if (CGRectContainsPoint(subview.frame, point))
                return YES;
    
    return NO;
}

@end
