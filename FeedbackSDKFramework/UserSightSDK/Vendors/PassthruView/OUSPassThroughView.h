//
//  OUSPassThroughView.h
//
//  Created by Torin Nguyen on 12/12/14.
//  Copyright (c) 2014 Originally US. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OUS_PASS_THROUGH_VIEW_TAGS                  59392

/*
 * This custom UIView allow touches to pass through any subviews
 * that has tag < OUS_PASS_THROUGH_VIEW_TAGS, including self
 */
@interface OUSPassThroughView : UIView

- (void)allowPassThrough:(BOOL)allowed;

@end
