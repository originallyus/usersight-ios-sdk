//
//  ViewController.swift
//  SampleApplicationSwift
//
//  Created by Torin Nguyen on 16/4/20.
//  Copyright © 2022 User Sight. All rights reserved.
//

import UIKit

import UserSightSDK

class DemoViewController: UIViewController {

    @IBOutlet var lblVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblVersion.text = "v\(UserSightSDK.version())"
    }

    @IBAction func onBtnLanguageEnglish(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("en")       //"en", "zh", "ms", "ta", "th", "vi"
    }
    
    @IBAction func onBtnLanguageChinese(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("zh")       //"en", "zh", "ms", "ta", "th", "vi"
    }
    
    @IBAction func onBtnLanguageMalay(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("ms")       //"en", "zh", "ms", "ta", "th", "vi"
    }
    
    @IBAction func onBtnLanguageThai(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("th")       //"en", "zh", "ms", "ta", "th", "vi"
    }
    
    @IBAction func onBtnLanguageIndonesia(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("id")       //"en", "zh", "ms", "ta", "th", "vi"
    }
    
    @IBAction func onBtnLanguageTraditionalChinese(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("zh_tw")       //"en", "zh", "ms", "ta", "th", "vi"
    }

    
    @IBAction func onBtnLanguageVietnamese(sender: UIButton) {
        //Optional: set language
        //Note: this will requires translation texts to be already available on our backend
        UserSightSDK.setLanguage("vi")       //"en", "zh", "ms", "ta", "th", "vi"
    }

    @IBAction func onBtnSetUserID(sender: UIButton) {

        //Obtain your app's UserID from somewhere else
        let userId = "1234";
        
        //Configure SDK to use this User ID in reports
        UserSightSDK.setUserId(userId)
    }
    
    @IBAction func onBtnSetMetadata(sender: UIButton) {

        //Obtain this metadata from somewhere else
        let metadata = "policy_8174519";
        
        //Configure UserSightSDK to use this metadata in reports
        UserSightSDK.setMetadata(metadata)
    }
    
    @IBAction func onBtnDebugFeedbackForm(sender: UIButton) {
        //This will always show the rating form in DEBUG mode, regardless of configurations on server side
        //EventTag is an optional event name/tag coming from your app. This is for reporting purpose only
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug") { didShow in
            print("didShow:", didShow)
        }
    }
    
    @IBAction func onBtnDebugAltFeedbackForm(sender: UIButton) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "alt_rating_form")
    }
    
    @IBAction func onBtnShowRatingSDK(sender: UIButton) {
        //This will check with backend whether it should be displayed or not
        //EventTag is an optional event name/tag coming from your app. This is for reporting purpose only
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_feedback")
    }
    
    // MARK: - Test / Debug
    @IBAction func onBtnDebugFormSatisfaction(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug") { didShow in
            print("didShow: \(NSNumber(value: didShow))")
        }

        //Starting from SDK version 0.3.x onwards, there are 6 different Survey Type available, configurable from DFS CMS (backend)
        //Please consult your business/admin team to know exactly which eventTag, formSlug to be used at which point of your User Journey

        // Sample code for force dismissing UI, to be used only for session timeout or session expired scenarios
        /*
             * Forcefully dismiss the rating UI
             * This may be suitable for scenarios like session timeout, session expired where host application
             * needs to forcefully dismiss any user-related UI and logout the user
             * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
             * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
             * Completion block will be called as per usual after finish dismissing the UI
             * This function can be called from any thread (both background thread & main thread)
             */
        /*
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [UserSightSDK forceDismiss:YES];
            });
             */
    }

    @IBAction func onBtnDebugFormNPS(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "nps-1")
    }
    
    @IBAction func onBtnDebugFormPoll(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "poll-1")
    }
    
    @IBAction func onBtnDebugFormComment(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "comment-1")
    }

    @IBAction func onBtnDebugFormCES(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "effort-1")
    }

    @IBAction func onBtnDebugFormExternal(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.debugFeedbackForm(withEventTag: "btn_debug_alt", formSlug: "external-1")
    }

    // MARK: - Test / Production
    @IBAction func onBtnProductionFormSatisfaction(_ sender: Any) {
        //A form may not be shown all the time, depends on the frequency configurations on our SDK backend.
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show") { didShow in
            print("didShow: \(NSNumber(value: didShow))")
        }

        //Starting from SDK version 0.3.x onwards, there are 6 different Survey Type available, configurable from DFS CMS (backend)
        //Please consult your business/admin team to know exactly which eventTag, formSlug to be used at which point of your User Journey
        //[UserSightSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"satisfaction-1"];
    }

    @IBAction func onBtnProductionFormNPS(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_alt", formSlug: "nps-1")
    }

    @IBAction func onBtnProductionFormPoll(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_alt", formSlug: "poll-1")
    }

    @IBAction func onBtnProductionFormComment(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_alt", formSlug: "comment-1")
    }

    @IBAction func onBtnProductionFormCES(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_alt", formSlug: "effort-1")
    }

    @IBAction func onBtnProductionFormExternal(_ sender: Any) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        UserSightSDK.showFeedbackForm(withEventTag: "btn_show_alt", formSlug: "external-1")
    }

}

