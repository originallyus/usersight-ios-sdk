//
//  AppDelegate.swift
//

import UIKit

import UserSightSDK

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //Initialize FeedbackSDK
        //Please contact us to obtain App Secret specific to your app's Bundle ID
        let appSecret = "4WWCXBMYwXRkk8gbWfPu"
        UserSightSDK.initWithAppSecret(appSecret, application: application)
        
        return true
    }
    
}

