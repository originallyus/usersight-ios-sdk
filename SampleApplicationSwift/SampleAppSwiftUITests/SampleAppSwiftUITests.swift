//
//  SampleAppSwiftUITests.swift
//  SampleAppSwiftUITests
//
//  Created by Tuan_Anh1 on 24/02/2021.
//  Copyright © 2022 User Sight. All rights reserved.
//

import XCTest

class SampleAppSwiftUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        // UI tests must launch the application that they test.
        app = XCUIApplication()
        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testOpenTestRating() throws {
        
        let app = XCUIApplication()
        app/*@START_MENU_TOKEN@*/.staticTexts["Test Rating form"]/*[[".buttons[\"Test Rating form\"].staticTexts[\"Test Rating form\"]",".staticTexts[\"Test Rating form\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        let feedbackSdkCloseButton = app.buttons["feedback sdk close"]
        let formShow = feedbackSdkCloseButton.waitForExistence(timeout: 1)
        XCTAssertTrue(formShow)
        feedbackSdkCloseButton.tap()
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testOpenTestAltRating() throws {
        let app = XCUIApplication()
        app/*@START_MENU_TOKEN@*/.staticTexts["Test Non-Instrusive Rating form"]/*[[".buttons[\"Test Non-Instrusive Rating form\"].staticTexts[\"Test Non-Instrusive Rating form\"]",".staticTexts[\"Test Non-Instrusive Rating form\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let feedbackSdkCloseButton = app.buttons["feedback sdk close"]
        let formShow = feedbackSdkCloseButton.waitForExistence(timeout: 1)
        XCTAssertTrue(formShow)
        feedbackSdkCloseButton.tap()
    }

//    func testLaunchPerformance() throws {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTApplicationLaunchMetric()]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
